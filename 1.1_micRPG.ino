//sudo chmod a+rw /dev/ttyUSB0
#include <U8g2lib.h>
//#include <math.h>
#include <EEPROM.h>
// EEPROM structure //
// (0,1): #1 best time, (2,3): #2 bt, (4,5): #3 bt
const uint8_t PROGMEM buttRight = 6;
const uint8_t PROGMEM buttLeft = 5;
const uint8_t PROGMEM buttJump = 8;
const uint8_t PROGMEM buttAction = 7;
const uint8_t PROGMEM buttPause = 9;

long gameTime = 0;
uint16_t finalTime = 0; // 2 bytes
U8G2_SSD1306_128X64_NONAME_F_HW_I2C SC(U8G2_R0, /* clock=*/ 5, /* data=*/ 4, /* reset=*/ U8X8_PIN_NONE);

byte nextDungeon = 0;
uint8_t dungeonsInZone = 0;
uint8_t groundMap[16];
uint8_t roofMap[16];
uint8_t theme = 255;
uint8_t flavour;
uint8_t zone = 3;

//uint8_t playerTileY;
#include "world_flavour.h"
#include "utilities.h"
#include "sprites.h"
#include "objectClass.h"
#include "charClass.h"
#include "projectileClass.h"
#include "itemClass.h"
#include "playerClass.h"
Player pl(/*tx*/0,/*ty*/6,/*ox*/0,/*oy*/0);
void(* resetFunc) (void) = 0;
#include "objectMethods.h"
#include "enemyClass.h"
#include "dungeonClass.h"

// SKELLY -> DIES -> TOMB -> DROP !!
// BARREL = CHEST
// TO DO LIST:
// > REDO NPC like OBJECTS (maybe allows to have 2 of them?)
// > Update Heart Art
// > Skelly Cave
// > Finale


// https://github.com/ThingPulse/esp8266-oled-ssd1306
// https://forum.arduino.cc/index.php?topic=37453.msg276515#msg276515
// https://github.com/jshaw/SimplexNoise
// https://softwareengineering.stackexchange.com/questions/247245/a-vector-of-pointers-to-different-derived-classes-of-the-same-base-class
// https://learn.sparkfun.com/tutorials/data-types-in-arduino/all
// https://stackoverflow.com/questions/9293089/changing-the-git-structure



void initialScreen() {
  text = 22;
  drawText(10, 10);
  text = 23;
  drawText(60, 60);

  int idleTime = 0;
  uint8_t sprite = 0;
  while (digitalRead(buttPause) == HIGH) {
    if (millis() / 1000 - idleTime > 1) {
      spriteRenderer(7, 0, 4, 2, sprite);
      if (sprite == 0) {
        sprite = 1;
      } else {
        sprite = 0;
      }
      idleTime = millis() / 1000;
      SC.sendBuffer();
    }
  }
  while (digitalRead(buttPause) == LOW) {};
  return;
}
uint8_t optionsScreen() {
  while (digitalRead(buttPause) == LOW) {};
  int delayTime = 0;
  uint8_t sprite = 0;
  uint8_t selectedOption = 0;
  int idleTime = 0;
  while (digitalRead(buttPause) == HIGH) {
    SC.clearBuffer();
    text = 24;
    drawText(20, 20);
    text = 25;
    drawText(20, 40);
    text = 21;
    drawText(20, 60);
    if (selectedOption == 0) {
      spriteRenderer(toTile(10), toTile(20), 0, -3, sprite);
    } else if(selectedOption == 1) {
      spriteRenderer(toTile(10), toTile(40), 0, -7, sprite);
    }else{
      spriteRenderer(toTile(10), toTile(60), 0, -3, sprite);
    }
    if (millis() / 1000 - idleTime > 1) {
      idleTime = millis() / 1000;
      if (sprite == 0) {
        sprite = 1;
      } else {
        sprite = 0;
      }
    }
    if (digitalRead(buttLeft) == LOW) {
      selectedOption -= 1;
    } else if (digitalRead(buttRight) == LOW) {
      selectedOption += 1;
    }
    selectedOption = selectedOption % 3;
    SC.sendBuffer();
    delay(100);
  }
  while (digitalRead(buttPause) == LOW) {};
  return selectedOption;
}
void creditsScreen() {
  SC.clearBuffer();
  text = 26;
  drawText(10, 10);
  text = 27;
  drawText(10, 20);
  text = 28;
  drawText(30, 30);
  text = 29;
  drawText(10, 40);
  text = 30;
  drawText(10, 50);
  text = 31;
  drawText(10, 60);
  SC.sendBuffer();
  while (digitalRead(buttPause) == HIGH) {};
  while (digitalRead(buttPause) == LOW) {};
}
void leaderboardScreen(){
  SC.clearBuffer();
  uint16_t eepromValue = 0;
  for(uint8_t i = 0; i < 3; ++i){
    SC.setCursor(20,20 + 20*i);
    EEPROM.get(2*i,eepromValue);
    SC.print(eepromValue%65535);
  }
  SC.sendBuffer();  
  while (digitalRead(buttPause) == HIGH) {};
  while (digitalRead(buttPause) == LOW) {};
}
long gameLastTime;
void optionsLoop(){
  uint8_t nextOption = 1;
  while (nextOption != 0) {
    nextOption = optionsScreen();
    if (nextOption == 1) {
      creditsScreen();
    }else if(nextOption == 2){
      leaderboardScreen();
    }
  }
  // RESET FOR NEW RUN
  text = 0;
  SC.setBitmapMode(1);
  gameTime = 0;
  pl.lives = 3; 
  zone = 3;
  randomSeed(analogRead(A3));
  //pl.resetPlayer(); !<o>!
  generateDungeon();
  gameLastTime = millis();
  return;
}



void setup() {
  //Serial.begin(9600);
  pinMode(buttPause, INPUT_PULLUP);
  pinMode(buttAction, INPUT_PULLUP);
  pinMode(buttJump, INPUT_PULLUP);
  pinMode(buttLeft, INPUT_PULLUP);
  pinMode(buttRight, INPUT_PULLUP);
  randomSeed(analogRead(A3));
  SC.begin();
  SC.setFont(u8g2_font_helvR08_tf);
  initialScreen();
  optionsLoop();
}

void loop() {
  if (pl.lives <= 0) {
    SC.clearBuffer();
    text = 44;
    drawText(10, 10);
    pl.drawHUD();
    SC.sendBuffer();
    while (digitalRead(buttPause) == HIGH) {};
    //optionsLoop();
    resetFunc();
  } else if (digitalRead(buttPause) == LOW && millis() - gameLastTime > 50) {
    SC.clearBuffer();
    gameTime = millis();
    pl.drawHUD();
    SC.sendBuffer();
    delay(10);
  } else if (millis() - gameLastTime > 50) {
    SC.clearBuffer();
    //playerTileY = pl.tileY;
    updateDungeon();
    pl.updatePlayer(groundMap, roofMap, objects, npc);
    if (nextDungeon == 1) {
      ++dungeonsInZone;
      for (int i = 0; i < 3; ++i) {
        delete objects[i];
      }
      pl.mapItem.sprite = 0; // delete floating items
      delete npc;
      text = 0;
      nextDungeon = 0;
      generateDungeon();
      pl.tileX = 0;
      pl.tileY = toTile(groundMap[0]);
      pl.offsY = toOffs(groundMap[0]);
    }
    SC.clearBuffer();
    drawDungeon();
    gameTime = millis();
    pl.drawPlayer();
    SC.sendBuffer();
    gameLastTime = millis();
  }
  //dg.updateDungeon();
  //dg.drawBackground();
  //dg.drawObjects();
  //dg.drawCharacters();
}
