# Micro RPG

This is a tiny fanzine videogame written for Arduino UNO, 64x128 OLED and 5 buttons. 

## Dungeon Posters

Tiny posters introducing some of the environments, rooms, objects and characters found throughout the game.

![Alt text](./posters/0_CASTLE_HALL.png)

The player advances through the dungeon, where the rooms are generated with random and secret elements.

![Alt text](./posters/DUNGEON_PLATFORM.png)

## The Console

The physical console for this project has been designed and 3D printed by Bernat Santaeugenia. 

This was my testing setup, with an Arduino Micro:

![Alt Text](posters/arduino_test_setup.jpeg)

Trying to fit all the electronics inside the casing. The buttons were quite challenging to design with resin 3D printed pieces only, but they ended up working. For the prototype we used an Arduino Nano.

![Alt Text](posters/casing_test.jpeg)

![Alt Text](posters/assembling.jpeg)

Finally, some gameplay on the final prototype!

![Alt Text](posters/gameplay.gif)

