class Character{
  public:
    uint8_t tileX;
    uint8_t tileY;
    int8_t offsX;
    int8_t offsY;
    int8_t vX = 0;
    int8_t vY = 0;
    uint8_t dir = 1; // 0 left, 1 right
    uint8_t lives;
    long animLastTime;
    // fps 
    uint8_t sprite;
    Character(uint8_t tx, uint8_t ty, uint8_t ox, uint8_t oy){
      tileX = tx;
      tileY = ty;
      offsX = ox;
      offsY = oy;
    }
    virtual uint8_t getSprite(){return 0;};
    virtual void getUpdate(){return;};
    void updatePosition(uint8_t (&groundPixels)[16], uint8_t (&roofPixels)[16], Object* cObj[3]);
    void correctCollisions(uint8_t (&groundPixels)[16], uint8_t (&roofPixels)[16], Object* cObj[3] );
    virtual void getHitBy(uint8_t att){return;}
};
void Character::updatePosition(uint8_t (&groundPixels)[16], uint8_t (&roofPixels)[16], Object* cObj[3] ){
  getUpdate(); // computes update in velocities
  // update position
  offsX += vX;
  tileX += offsX/8;
  if(offsX < 0){
    offsX = -(abs(offsX)%8);
  }else{
    offsX = offsX%8;
  }
  // update position
  offsY += vY;
  tileY += offsY/8;
  if(offsY < 0){
    tileY = tileY - 1;
    offsY = 8+offsY-1;
  }else{
    offsY = offsY%8;
  }
  correctCollisions(groundPixels, roofPixels, cObj);
}
void Character::correctCollisions(uint8_t (&groundPixels)[16],uint8_t (&roofPixels)[16], Object* cObj[3] ){
  uint8_t yPos = toPixel(tileY,offsY) + 8; //+vY;?
  // object collision
  for(uint8_t i = 0; i < 3; ++i){
    if(abs(cObj[i]->tileX - tileX) < 2 && abs(cObj[i]->tileY - tileY) < 2){
      uint8_t collDirection = cObj[i]->isCollidingWith(toPixel(tileX,offsX)+3,toPixel(tileY,offsY),2,7);
      switch(collDirection){
        case 0:
          break;
        case 1: // correct to the left
          tileX = cObj[i]->tileX - 1;
          offsX = 2;
          vX = 0;
          break;
        case 2: // correct to the right
          tileX = cObj[i]->tileX + 1;
          offsX = -2;
          vX = 0;
          break;
        case 3: // correct to the top
          tileY = cObj[i]->tileY-1;
          offsY = cObj[i]->offsY; 
          vY = 0;
          break;
        case 4: // correct to the bott
          tileY = cObj[i]->tileY;
          offsY = -cObj[i]->offsY; 
          vY = 0;
          getHitBy(cObj[i]->sprite); // objects that fall on top deal dmg
          break;
      }
    }
  }
  // no escaping through the left
  if(toPixel(tileX,offsX) < -2){
    tileX = 0;
    offsX = -2;
    vX = 0;
  }else if(tileX == 15 && offsX > 2){ //next dungeon to the right
    tileX = 0;
    offsX = -2;
    nextDungeon = 1;
    return;
  } 
  if(offsX > 2 && tileX <= 15) {
    if(yPos - 3 <= groundPixels[tileX + 1]){
      // right fall/walk
      tileX += 1;
      offsX = -5 + offsX;
    }else {//yPos > groundPixels[tileX + 1]
    // right wall 
      offsX = 2;
      vX = 0;
    }
  }else if(offsX < -2 && tileX >= 0)
    if(yPos - 3 <= groundPixels[tileX - 1]){
      // left fall/walk
      tileX -= 1;
      offsX = 5 + offsX;
   }else{ // yPos > groundPixels[tileX - 1])
      // left wall 
      offsX = -2;
      vX = 0;
  }

  // FLOOR WALLS ///
  // !<O>!
  // ground
  if(yPos > groundPixels[tileX]){
    tileY = groundPixels[tileX]/8-1; 
    offsY = groundPixels[tileX]%8;
    vY = 0;
  } else if(yPos - 8 < roofPixels[tileX]){ // floor
    tileY = roofPixels[tileX]/8+1; 
    offsY = roofPixels[tileX]%8-8;
    vY = 0;    
  }
}