uint8_t columns[5] = {255,255,255,255,255};
Object* objects[3]; //{new Object(0,0,0,0), new Object(0,0,0,0), new Object(0,0,0,0)};
Character* npc;

void updateLeaderboard(uint16_t ft){
  uint16_t eepromValue = 0;
  for(uint8_t i = 0; i < 3; ++i){
    EEPROM.get(0+2*i,eepromValue);
    if(eepromValue%65535 < ft){
      EEPROM.put(2*i,ft);
      break;
    }
  }
}
void debugSetTheme(){ 
  // priest(30) -> dungeon_corridor(0) -> skelly_cave(1) -> platform(2) -> eye(111) -> castle_hall(11) -> castle_drain(12) -> castle_lab(17) -> finale(254) -> endScreen(222)
  if(theme == 255){theme = 30;}
  else{theme = 11;flavour = 0;}
  /*if(theme == 255){theme = 30;}
  else if(theme == 30){theme = 0;}
  else if(theme == 0){theme = 1;}
  else if(theme == 1){theme = 2;}
  else if(theme == 2){theme = 111;}
  else if(theme == 111){theme = 11;}
  else if(theme == 11){theme = 12;}
  else if(theme == 12){theme = 17;}
  else if(theme == 17){theme = 254;}
  else{theme = 222;}*/
}
void setTheme(){
  switch(zone){
    case 0: // DUNGEON
    {
      uint8_t rand = random(1,100);
      if(rand == 69){theme = 111;}
      else if(rand > 50){ theme = 0; } // CORRIDOR 
      else if(rand > 10){ theme = 1; }// SKELLY
      else{ theme = 2; } 
      if(dungeonsInZone > 5){
        zone = 1;
        dungeonsInZone = 0;
      }
      // theme = 254;
      break;
    }
    case 1: // CASTLE
      if(theme == 14){
        theme = 12; // drain
        --dungeonsInZone; 
      }else if(theme == 12){
        --dungeonsInZone; // the drain does not count as dungeon
        if(random(1,100)>70){
          theme = 11;
          ++dungeonsInZone;
        }
      }
      else if(random(1,100) > 90){theme = 17;} // castle laboratory
      else{theme = 11;} // castle lobby
      if(dungeonsInZone > 5){
        zone = 4; 
      }
      break;
    case 2: // SWAMP
      break;
    case 3: // PRIEST SCENE
      theme = 30;
      zone = 0;
      dungeonsInZone = 0;
      break;
    case 4: // FINAL CUTSCENES
      if(theme != 254 && theme != 222){
        theme = 254;
      }else if(theme == 254){
        theme = 222;
        finalTime = gameTime/1000;
        updateLeaderboard(finalTime);
      }else if(theme == 222){
        resetFunc();
      }else{
        theme = 222;
      }
      break;
  }
  //theme = 0;
}
void setFlavour(){
  if(theme == 11){
    // 0 : Knight + Chest
    // 1 : Dinner Table
    // 2 : Trap Chest
    uint8_t rand = random(0,100);
    if(rand > 50){flavour = 0;}
    else{flavour = 1;}
    flavour = 1;
  }else if(theme == 1){
    // flavour encodes the position of the flowers
    flavour = random(2,9);
  }
}
void generateNPC(){
  uint8_t oi = 0;
  switch(theme){
    case 0: // DUNGEON CORRIDOR
      // Bat
      npc = new NPC(2,9,9,0,0);
      break;
    case 1: // DUNGEON SKELLY
      {
        uint8_t auxTile = random(5,14);
        // Skelly
        npc = new NPC(1,auxTile,toTile(groundMap[auxTile])-1,0,toOffs(groundMap[auxTile]));
        //debugPrint(npc->getSprite(),10,10);
        //delay(1000);
        break;
      }
    case 11:
      // Knight
      if(flavour == 0){npc = new NPC(4,10,toTile(groundMap[10])-1,0,toOffs(groundMap[10]));}
      else{npc = new Character(0,0,0,0);}
      break;  
    case 12:
      // Snake  
      npc = new NPC(5,10,toTile(groundMap[10])-1,0,toOffs(groundMap[10]));
      break;
    case 111:
    // Eye
      npc = new NPC(3,7,0,0,3);
      //npc = new Character(0,0,0,0);
      break;
    default:
      npc = new Character(0,0,0,0);
      break;
  }
}
void generateObjects(){
  uint8_t oi = 0;
  switch(theme){
    case 0: // DUNGEON CORRIDOR
    {
      for(uint8_t i = 2; i < 15; ++i){
        if(random(1,100) > 85 && oi < 3){
          // Chest
          objects[oi] = new Object(1,i,toTile(groundMap[i])-1,0,toOffs(groundMap[i]));
          ++oi;
        }
      } 
      break;
    }
    case 2: 
      oi = 2;
      // Chain Platform
      objects[0] = new Object(6,12,4,0,0); // objective = 2
      //objects[1] = new chainPlatform(6,2,6,0,-3); // objective = 13
      objects[1] = new Object(7,14,2,0,0); // Lever
      break;
    case 11:
      if(flavour == 0){
        if(random(1,100) > 80){
          // Drain Entrance
          objects[oi] = new Object(8,8,toTile(groundMap[8])-1,0,toOffs(groundMap[8])-4);
          ++oi;
        }
        if(random(1,100) > 80){
          uint8_t i = random(3,13);
          // Chest
          objects[oi] = new Object(1,i,toTile(groundMap[i])-1,0,toOffs(groundMap[i]));
          ++oi;
        }
      }else if(flavour == 1){
        // Turkey
        objects[oi] = new Object(10,6,5,4,6);
        ++oi;
      }
      break;
    case 12:
      for(uint8_t i = 2; i < 15; ++i){
        if(oi < 3 & groundMap[i]-roofMap[i] > 16 & groundMap[i-1]-roofMap[i-1] > 12 & random(1,100)>90){
          // Barrel
          objects[oi] = new Object(2,i,toTile(groundMap[i])-1,0,toOffs(groundMap[i]));
          ++oi;
        }
      }
      break;
    case 17:
      {
      // Telescope
      objects[0] = new Object(9,4,toTile(groundMap[4])-1,0,toOffs(groundMap[4])-3);
      // Lab Globe
      objects[1] = new Object(4,12,toTile(groundMap[12])-1,0,toOffs(groundMap[12])-3);
      uint8_t auxTile = random(6,11);
      // Chest
      objects[2] = new Object(1,auxTile,toTile(groundMap[auxTile])-1,0,toOffs(groundMap[auxTile]));
      oi = 3;}
      break;
    case 30: // INITIAL PRIEST SCENE
    {
      // Priest
      objects[oi] = new Object(3,12,toTile(groundMap[12])-1,0,toOffs(groundMap[12]));
      ++oi;
      break;
    }
    case 111:
      for(uint8_t i = 0; i < 3; ++i){
        // Teardrop
        objects[i] = new Object(5,random(2,14),0,0,0);
      }
      oi = 3;
      break;
  }
  for(int i = oi; i < 3; ++i){
    objects[i] = new Object(0,0,0,0,0);
  }
}
void slopedFloor(){
  groundMap[0] = 50;
  uint8_t floorDelta = 0;
  for(int i = 1; i < 16; ++i){
    floorDelta = random(-2,3);
    groundMap[i] = groundMap[i-1] + floorDelta;
  }
}
void threeChopsFloor(){
  groundMap[0] = 50;
  groundMap[15] = 0;
  roofMap[15] = 0;
  for(int i = 2; i < 15; ++i){
    groundMap[i] = 64;
  }
  if(random(1,100) > 60){
    groundMap[random(4,12)] = random(45,55);
  }
}
void slopedRoof(){
  roofMap[0] = 2;
  uint8_t roofDelta = 0;
  for(int i = 1; i < 16; ++i){
    roofDelta = random(-2,3);
    roofMap[i] = roofMap[i-1] + roofDelta;
    if(roofMap[i] < 1 || roofMap[i] > 200){
      roofMap[i] = 1;
    }
  }
}
void waterChoppyFloor(){
  for(int i = 0; i < 16; ++i){
    groundMap[i] = random(45,56);
    if(i > 1 && random(1,100) > 90){ //water drops
      groundMap[i] = 64;
    }
  }
}
void normalRoof(){
  for(int i = 0; i < 16; ++i){
    roofMap[i] = random(8,16);
  }
}
void flatFloor(){
  for(int i = 0; i < 16; ++i){
    groundMap[i] = 60; 
  }
}
void flatRoof(){
  for(int i = 0; i < 16; ++i){
    roofMap[i] = 1; 
  }
}
void merlonRoof(){
  for(int i = 0; i < 16; ++i){
    if(i%2 == 1){
      roofMap[i] = 2;
    }else{
      roofMap[i] = 10;
    } 
  }
}
void drainFloorRoof(){
  uint8_t tunnel_length = 0;
  uint8_t level = 20;
  for(uint8_t i = 0; i < 15; ++i){
    if(tunnel_length > 1 & random(1,100) > 50){
      tunnel_length = 0;
      if(level < 30){
        level += random(5,20);
      }else{
        level -= random(5,20);
      }
    }
    ++tunnel_length;
    roofMap[i] = level - random(-2,2);
  }
  roofMap[15] = roofMap[14];
  groundMap[0] = roofMap[0] + 10;
  for(uint8_t i = 1; i < 15; ++i){
    if(roofMap[i+1]<roofMap[i-1]){// tunnel up
      groundMap[i] = roofMap[i-1] + random(9,12);
    }else{
      groundMap[i] = roofMap[i+1] + random(9,12);
    }
  }
  groundMap[15] = roofMap[15] + 10;
  // spawn rooms
  if(random(1,100) > 1){
    uint8_t rand = random(3,10);
    for(uint8_t i = rand; i <rand+random(2,4); ++i){
      groundMap[i] += random(8,16);
    }
  }
}
void generateGroundAndFloor(){
  switch(theme){
    case 0: // DUNGEON CORRIDOR
      waterChoppyFloor();
      normalRoof();
      break;
    case 1: // DUNGEON SKELLY
      slopedFloor();
      normalRoof();      
      break;
    case 2:
      slopedRoof();
      threeChopsFloor();
      break;
    case 11: // CASTLE CORRIDOR
      flatFloor();
      merlonRoof();
      // dining table
      if(flavour == 1){
        groundMap[6] -= 6;
        groundMap[7] -= 6;
        groundMap[8] -= 6;
      }
      break;
    case 17: // CASTLE LAB
      flatFloor();
      merlonRoof();
      break;
    case 12:
      drainFloorRoof();
      break;
    case 30: // PRIEST
      slopedFloor();
      normalRoof();     
      break; 
    case 111: // EYE
      slopedFloor();
      flatRoof();     
      break;
    case 254:
      flatFloor();
      flatRoof();
      break;
          
  }
}
void generateDecorations(){
  for(int i = 0; i < 5; ++i){
    columns[i] = 255;
  }
  
  switch(theme){
    case 0: // DUNGEON CORRIDOR
    {
      uint8_t k = 0; 
      for(int i = 1; i < 16; ++i){
        if(random(1,100) > 90 & groundMap[i] != 64){
          columns[k] = i;
          ++k;
          roofMap[i] = groundMap[i] - 24;
        }
      }
      break;
    }
    case 11:
    {
      columns[0] = 3;
      columns[1] = 8;
      columns[2] = 13;
      break;
    }
    case 17:
      columns[0] = 3;
      columns[1] = 13;
      break;    
    default:
      break;
  }

}
void generateDungeon(){
  // Draw theme of dungeon
  //setTheme();
  debugSetTheme();
  // setFlavour();
  // Generate Ground & Floor
  generateGroundAndFloor();
  // Decorations
  generateDecorations();
  // Objects
  generateObjects();
  // NPCs
  generateNPC();
}
uint8_t columnSprite(uint8_t bmt){ //0: bottom, 1: middle, 2: top
  // choose sprite according to theme
  switch(theme){
    case 0: // Dungeon
    {
      switch(bmt){
        case 0:
          return 55;
        case 1:
          return 56;
        case 2: 
          return 57;
      }
      break;
    }
    case 11:
      if(bmt == 2){
        return 201;
      }else{
        return 255;
      }
      break;
    case 17:
      if(bmt == 2){
        return 201;
      }else{
        return 255;
      }
      break;
    default:
      break;
  }
  return 255;
}
void drawNPC(){
  if(npc->getSprite() == 0){
    return;
  }else{
    uint8_t x = toPixel(npc->tileX,npc->offsX) + 2;
    uint8_t y = toPixel(npc->tileY,npc->offsY) - 2;
    for(int i = 0;  i < npc->lives; ++i){
      SC.drawPixel(x,y);
      ++x;
      ++x;
    } 
    uint8_t sp = npc->getSprite();
    spriteRenderer(npc->tileX,npc->tileY,npc->offsX,npc->offsY,sp);
    if(sp == 111 || sp == 113 || sp == 115){
      spriteRenderer(npc->tileX + 1,npc->tileY,npc->offsX,npc->offsY,sp+1);
    }

  }
  return;
}
void drawObjects(){
  for(int i = 0; i < 3; ++i){
    Object* o = objects[i];
    if(o->sprite == 0){
      return;
    }else{
    // SC.setCursor(100,10);
    //SC.print("hello");
    spriteRenderer(o->tileX,o->tileY,o->offsX,o->offsY,o->getSprite());
    }
  }
  return;
}
void drawDungeon(){
  // Floor
  for(int j = 0; j < 15; ++j){
    if(groundMap[j] < 64){
      SC.drawHLine(j*8,groundMap[j],8);
    }else{ // water
      spriteRenderer(j,7,0,0,101);
    }
    
    if(groundMap[j+1] != groundMap[j]){
      SC.drawLine(j*8+8,groundMap[j],j*8+8,groundMap[j+1]);
    }
  }
  SC.drawHLine(15*8,groundMap[15],8);
  // Roof
  for(int j = 0; j < 15; ++j){
    SC.drawHLine(j*8,roofMap[j],8);
    if(roofMap[j+1] != roofMap[j]){
      SC.drawLine(j*8+8,roofMap[j],j*8+8,roofMap[j+1]);
    }
  }
  SC.drawHLine(15*8,roofMap[15],8);
  drawObjects();
  drawNPC();
  if(theme == 1){
    uint8_t spriteMod = millis()/700%4;
    if(spriteMod == 3){spriteMod = 1;}
    spriteRenderer(flavour,toTile(groundMap[flavour])-1,0,toOffs(groundMap[flavour]),76+spriteMod);
    spriteMod = (millis()/650)%4;
    if(spriteMod == 3){spriteMod = 1;}
    spriteRenderer(flavour + 3,toTile(groundMap[flavour + 3])-1,0,toOffs(groundMap[flavour + 3]),76+spriteMod);
    spriteMod = (millis()/600)%4;
    if(spriteMod == 3){spriteMod = 1;}
    spriteRenderer(flavour + 7,toTile(groundMap[flavour + 7])-1,0,toOffs(groundMap[flavour + 7]),76+spriteMod);
  }
  else if(theme == 11 and flavour == 1){
    spriteRenderer(5,7,0,-4,75); // left chair
    spriteRenderer(9,7,1,-4,74); // right chair
    spriteRenderer(8,6,1,-2,67); // jar
  }
  else if(theme == 17){
    if(objects[0]->sprite == 226){
      spriteRenderer(7,3,4,0,224);
    }else{
      spriteRenderer(7,3,4,0,225);
    }
  }
  else if(theme == 254){
    spriteRenderer(9,1,0,4,254);
  }
  else if(theme == 222){
    SC.setCursor(40,40);
    SC.print(finalTime);
  }
  // Columns
  for(int i = 0; i < 5; ++i){
    if(columns[i] != 255){
      uint8_t yPos = groundMap[columns[i]];
      switch(theme){
        case 11:
          yPos -= 16;
          break;
        case 17:
          yPos -= 16;
          break;
        default: 
          break;
      }
      spriteRenderer(columns[i],(yPos-8)/8,0,(yPos-8)%8,columnSprite(0));
      spriteRenderer(columns[i],(yPos-16)/8,0,(yPos-16)%8,columnSprite(1));
      spriteRenderer(columns[i],(yPos-24)/8,0,(yPos-24)%8,columnSprite(2));
    }
  }
}
void updateDungeon(){
  if(theme == 111||theme == 2){
    for(int i = 0; i < 3; ++i){
      objects[i]->update(groundMap);
    }
  }
  if(npc->sprite != 0){
    npc->updatePosition(groundMap, roofMap, objects);
  }
}
