// NPC Ids:
// 0 : Empty
// 1 : Skelly
// 2 : Bat
// 3 : Eye
// 4 : Knight
// 5 : Snake

class NPC: public Character{
  public:
    uint8_t isAttacking = 0;
    int lastAttackTime = 0;
    uint8_t npcId;
    NPC(uint8_t id, uint8_t tx, uint8_t ty, uint8_t ox, uint8_t oy):Character(tx, ty, ox, oy){
      npcId = id;
      vX = 0;
      vY = 0;
      animLastTime = millis();
      dir = 0;
      switch(npcId){
      case 0: // Placeholder
        sprite = 0;
        break;
      case 1: // Skelly
        sprite = 17;
        lives = 3;
        break;
      case 2: // Bat
        vY = -3;
        lives = 1;
        break;
      case 3: // Eye
        sprite = 111;
        lives = 10;
        break;
      case 4: // Knight
        sprite = 204;
        lives = 6;
        break;
      case 5: // Snake
        sprite = 120;
        lives = 2;
        break;
      default:
        break;
      }
    }
    uint8_t getSprite();
    void getUpdate();
    void getHitBy(uint8_t att);
    void onDeathEffect();
    void attackPlayer();
};
uint8_t NPC::getSprite(){
  if(sprite == 0){ // DED
    return 0;
  }
  switch(npcId){
    case 1: // Skelly
      {
        if(vX == 0){
          if(millis() - animLastTime > 1000){ 
            animLastTime = millis();
            if(dir == 1){
              if(sprite == 16){
                sprite = 15;
              }else{
                sprite = 16;
              }
            }
            if(dir == 0){
              if(sprite == 18){
                sprite = 17;
              }else{
                sprite = 18;
              }
            }
          }
        }else if(vX != 0 && millis() - animLastTime > abs(vX)*(800-abs(vX)*200)){
            animLastTime = millis();
            if(vX > 0){// RUN RIGHT
              dir = 1;
              if(sprite == 19){
                sprite = 20;
              }else{
                sprite = 19;
              }  
            }else if(vX < 0){// RUN LEFT
              dir = 0;
              if(sprite == 21){
                sprite = 22;
              }else{
                sprite = 21;
              }  
            }   
        }
      }
      isAttacking = 0;
      break;
    case 2: // Bat
      {
        if(millis() - animLastTime > 333){
          animLastTime = millis();
          if(vX > 0){// RUN RIGHT
            dir = 1;
            if(sprite == 47){
              sprite = 48;
            }else{
              sprite = 47;
            }  
          }else if(vX < 0){// RUN LEFT
            dir = 0;
            if(sprite == 45){
              sprite = 46;
            }else{
              sprite = 45;
            }  
          }   
        }
      }
      isAttacking = 0;
      break;
    case 3: // Eye
      {
        if(millis() - animLastTime > 333){
            animLastTime = millis();
          if(pl.tileX <= 5){
            sprite =  113;
          }else if(pl.tileX >= 11){
            sprite = 115;
          }else{
            sprite = 111;
          }
        }
      }
      isAttacking = 0;
      break;
    case 4: // Knight
      {
        if(isAttacking > 0){
          if(dir == 0){ // looking left
            if(isAttacking == 1){sprite = 206; isAttacking = 2;}
            else if(isAttacking == 2){sprite = 207; isAttacking = 3;}
            else{sprite = 208; isAttacking = 0;}
          }else{ // looking right ;)
            if(isAttacking == 1){sprite = 209; isAttacking = 2;}
            else if(isAttacking == 2){sprite = 210; isAttacking = 3;}
            else{sprite = 211; isAttacking = 0;}
          }
        }
        else if(vX == 0){
          if(millis() - animLastTime > 1000){ 
            animLastTime = millis();
            if(dir == 1){
              if(sprite == 203){
                sprite = 202;
              }else{
                sprite = 203;
              }
            }
            if(dir == 0){
              if(sprite == 205){
                sprite = 204;
              }else{
                sprite = 205;
              }
            }
          }
        }
      }
      break;
    case 5: // Snake
      isAttacking = 0;
      break;
    default:
      {
        if(vX == 0){
          if(millis() - animLastTime > 1000){ 
            animLastTime = millis();
            if(dir == 1){
              if(sprite == 122){sprite = 121;}
              else{sprite = 122;}
            }
            if(dir == 0){
              if(sprite == 120){sprite = 119;}
              else{sprite = 120;}
            }
          }
        }else if(vX != 0 && millis() - animLastTime > abs(vX)*(800-abs(vX)*200)){
            animLastTime = millis();
            if(dir == 1){// RUN RIGHT
              //dir = 1;
              if(sprite == 122){sprite = 126;}
              else if(sprite == 126){sprite = 127;}
              else if(sprite == 127){sprite = 128;}
              else{sprite = 122;}  
            }else if(dir == 0){// RUN LEFT
              //dir = 0;
              if(sprite == 120){sprite = 123;}
              else if(sprite == 123){sprite = 124;}
              else if(sprite == 124){sprite = 125;}
              else{sprite = 120;}  
            }  
        }
      }
      break;
  }
  return sprite;
}
void NPC::getUpdate(){
  //SKELLY & KNIGHT & SNAKE
  if(npcId == 1 or npcId == 4 or npcId == 5){
    if(random(1,100) > 90){
      vY -= 3;
    }
    if((pl.tileX + 1 == tileX || pl.tileX - 1 == tileX) && pl.tileY == tileY && millis()/1000 - lastAttackTime >= 2){
      attackPlayer();
      lastAttackTime = millis()/1000;
      vX = 0;
    } 
    else if(isAttacking > 0){
      vX = 0;
    }   
    else if(pl.tileX < tileX){
      dir = 0;
      if(vX > -1){vX -= 1;}
    }else if(pl.tileX > tileX){
      dir = 1;
      if(vX < 1){vX += 1;}
    }else{
      vX = 0;
      /*if(pl.tileY == tileY && millis()/1000 - lastAttackTime >= 2){
        attackPlayer();
        lastAttackTime = millis()/1000;
      }*/
    }
    vY += 1;    
  } 
  else if(npcId == 2){ // Bat
    if(pl.tileX < tileX){
      dir = 0;
      if(vX >= 0){vX -= 1;}
    }else if(pl.tileX > tileX){
      dir = 1;
      if(vX <= 0){vX += 1;}
    }
    vY -= 1;
    if(tileY < 4){
      vY += 2;
    }
  }
  else if(npcId == 3){ // Eye
    if(lives < 4 && random(0,100) > 90){
      lives += 1;
    }
    vY = 0;
    }
}
void NPC::getHitBy(uint8_t att){
  switch(att){
    case 53: // sword
      lives -= 1;
      break;
    case 70: // staff
      lives -= 1;
      break;
    case 71: // bow
      lives -= 1;
      break;    
    default:
      break;
  }
  if(lives == 0){ // F
    onDeathEffect();
    sprite = 0;
    tileX = 0;
    tileY = 0;
  }
}
void NPC::onDeathEffect(){
  switch(npcId){
    case 0: // Placeholder
      break;
    case 1: // Skelly
    {
     uint8_t lootItem = 0;
      if(random(0,100) > 80){lootItem = 52;}// key
      else{lootItem = 79;} // bone
      pl.mapItem = Item(lootItem,tileX, tileY-1,offsX,offsY);
      break;
    }
    case 2: // Bat
      break;
    case 3: // Eye
      break;
    case 4: // Knight
      break;
    case 5: // Snake
      break;
    default:
      break;
  }
}
void NPC::attackPlayer(){
  isAttacking = 1;
  pl.getHitBy(npcId);
}