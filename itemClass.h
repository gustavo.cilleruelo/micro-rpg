
class Item: public Character{
  public:
    Item(uint8_t itemId, uint8_t tx, uint8_t ty, uint8_t ox, uint8_t oy):Character(tx, ty, ox, oy){
      sprite = itemId;
      vY = -3;
      if(tileX > 7){
        vX = -6;
      } else {
        vX = 6;
      }
      
    }
    void getUpdate();
    uint8_t getSprite();
    //void pickUp(Player* pl);
};
uint8_t Item::getSprite() { 
  return sprite;
}
void Item::getUpdate(){
  //if(vY < 0){
  vY += 1;  
  //}
  if(vX > 0){
    vX -= 1;
  } else if (vX < 0){
    vX += 1;
  }
}
