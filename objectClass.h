/*
  switch (objId){
    case 1: // Chest
      break;
    case 2: // Barrel
      break;
    case 3: // Priest
      break;
    case 4: // Lab Globe
      break;
    case 5: // Teardrop
      break;
    case 6: // Chain Platform
      break;
    case 7: // Lever
      break;
    case 8: // Drain Entrance
      break;
    case 9: // Telescope
      break;
    case 10: // Turkey Dish
      break;
    default:
      break;
  }
*/
// Chest : 1
// Barrel : 2
// Priest : 3
// Lab Globe : 4
// Teardrop : 5
// Chain Platform : 6
// Lever : 7
// Drain Entrance : 8
// Telescope : 9
// Turkey Dish : 10
uint8_t getPriestLoot(){
  uint8_t rand = random(0,100);
  uint8_t loot = 70;
  if(rand > 66){loot = 70;} // staff
  else if(rand > 33){loot = 71;} // bow
  else{loot = 53;} // sword
  return loot;
}
class Object{
  public:
    uint8_t loot = 255; // loot == 255 -> non-Interactive object
    uint8_t tileX;
    uint8_t tileY;
    int8_t offsX;
    int8_t offsY;
    uint8_t sprite = 0;
    uint8_t objId;
    long animLastTime = 0;
    uint16_t lastInteractionTime = 0;
    // auxiliary variable to track internal status changes of some objects
    uint8_t status = 0; // 0 closed, 1 open
    Object(uint8_t id, uint8_t tx, uint8_t ty, uint8_t ox, uint8_t oy){
      tileX = tx;
      tileY = ty;
      offsX = ox;
      offsY = oy;
      objId = id;
      switch (objId){
        case 1: // Chest
          sprite = 58;
          loot = randomLoot(theme,sprite);
          status = 0;
          break;
        case 2: // Barrel
          sprite = 200;
          loot = randomLoot(theme,sprite);
          break;
        case 3: // Priest
          sprite = 23;
          loot = getPriestLoot();
          break;
        case 4: // Lab Globe
          sprite = 228;
          break;
        case 5: // Teardrop
          if(tileX < 8){sprite = 117;}
          else{sprite = 118;}
          break;
        case 6: // Chain Platform
          sprite = 96;
          // status is objective tile where the platform slides to
          if(tileX == 12){status = 2;}
          else{status = 13;}
          break;
        case 7: // Lever
          sprite = 95;
          loot = 0;
          break;
        case 8: // Drain Entrance
          sprite = 95;
          loot = 0;
          break;
        case 9: // Telescope
          sprite = 226;
          loot = 0;
          status = 0;
          break;
        case 10: // Turkey Dish
          sprite = 72;
          loot = 0;
          status = 0;
          break;
        default:
          break;
      }
    }
    uint8_t isCollidingWith(uint8_t pPx, uint8_t pPy, uint8_t pPw, uint8_t pPh);
    uint8_t getSprite();//{return 0;}
    uint8_t interact();//{return 255;}
    uint8_t returnLoot();
    void update(uint8_t groundPixels[16]);//{return;}
};
uint8_t Object::isCollidingWith(uint8_t pPx, uint8_t pPy, uint8_t pPw, uint8_t pPh){
  // return 0: no collision
  // return 1: correct collision to the left
  // return 2: correct collision to the rigth
  // return 3: correct collision to the top
  // return 4: correct collision to the bottom 
  uint8_t sPx = toPixel(tileX, offsX);
  uint8_t sPy = toPixel(tileY, offsY);
  uint8_t sPw = 7;
  uint8_t sPh = 7;
  // collision distances
  int rightColl = sPx + sPw - pPx;
  int leftColl = pPx + pPw - sPx; 
  int bottColl = sPy + sPh - pPy;
  int topColl = pPy + pPh - sPy;
  //if(!(sPx + 8 >= pPx && sPx <= pPx + pPw && sPy + 8 >= pPy && sPy <= pPy + pPh)){
  if(!(leftColl >= 0 && rightColl >= 0 && bottColl >= 0 && topColl >= 0)){
    return 0;
  }
   else if(topColl <= rightColl && topColl <= bottColl && topColl <= leftColl){
    return 3;
  } else if(bottColl <= rightColl && bottColl <= topColl && bottColl <= leftColl){
    return 4;
  } else if(leftColl <= rightColl && leftColl <= topColl && leftColl <= rightColl){
    return 1;
  }
  return 2;
}

