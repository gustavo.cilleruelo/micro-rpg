// had to put the methods here so they have access to the player
// next game i make i will think things through better ;)

uint8_t Object::returnLoot(){
  uint8_t rl = 0;
  if(loot != 0){
    rl = loot;
    loot = 0;
  }
  return rl;
}
uint8_t Object::getSprite() {
  switch (objId){
    case 1: // Chest
      if(status == 1){sprite = 59;}
      else{sprite = 58;}
      break;
    case 2: // Barrel
      sprite = 200;
      break;
    case 3: // Priest
      if(millis()-animLastTime > 1300){
        animLastTime = millis();
        if(sprite == 23){sprite = 24;}
        else{sprite = 23;}
      }
      break;
    case 4: // Lab Globe
      if(millis()-animLastTime > 1300){
        animLastTime = millis();
        if(sprite == 228){sprite = 229;}
        else if(sprite == 229){sprite = 230;}
        else{sprite = 228;}
      }
      break;
    case 5: // Teardrop
      sprite = 117;
      break;
    case 6: // Chain Platform
      if(tileX < status){ // right
        sprite = 99;
      }else{ // left
        sprite = 98;
      }
      break;
    case 7: // Lever
      if(status == 0){sprite = 95;}
      else{sprite = 94;}
      break;
    case 8: // Drain Entrance
      if(status == 0){sprite = 222;}
      else{sprite = 223;}
      break;
    case 9: // Telescope
      if(status == 1){sprite = 227;}
      else{sprite = 226;}
      break;
    case 10: // Turkey Dish
      if(status == 0){sprite = 72;}
      else{sprite = 73;}
      break;
    default:
      break;
  }
  return sprite;
}
uint8_t Object::interact(){
  switch (objId){
    case 1: // Chest
      if(millis()/1000 - lastInteractionTime < 1){
        return 0;
      }
      lastInteractionTime = millis()/1000;
      if(status == 1){status = 0;} 
      else {status = 1;}     
      return returnLoot();
      break;
    case 2: // Barrel
      return returnLoot();
      break;
    case 3: // Priest
      if(millis()/1000 - lastInteractionTime < 2){
        return 0;
      }
      lastInteractionTime = millis()/1000;
      //priestExplainMission();
      return returnLoot();
      break;
    case 4: // Lab Globe
      break;
    case 5: // Teardrop
      break;
    case 6: // Chain Platform
      break;
    case 7: // Lever
      if(millis()/1000 - lastInteractionTime < 2){
        return 0;
      }
      lastInteractionTime = millis()/1000;
      if(status == 0){ // activate lever
        status = 1;
        groundMap[15] = 50;
        roofMap[15] = 40;
      }else{
        status = 0;
        groundMap[15] = 0;
        roofMap[15] = 0;
      }
      break;
    case 8: // Drain Entrance
      if(millis()/1000 - lastInteractionTime < 2){
        return 0;
      }
      lastInteractionTime = millis()/1000;
      if(status == 0){ // closed door
        status = 1;
      }else{ // open door -> go to theme 12 (drain), by 14 theme marker
        status = 0;
        theme = 14;
        nextDungeon = 1;
      }
      return 0;
      break;
    case 9: // Telescope
      if(millis()/1000 - lastInteractionTime < 2){
        return 0;
      }
      lastInteractionTime = millis()/1000;
      if(status == 1){status = 0;}
      else{status = 1;}
      return 0;
      break;
    case 10: // Turkey Dish
      if(status == 0){
        status = 1;
        if(pl.lives < pl.maxLives){
          pl.lives += 1;
        }
      }
      return 0;
      break;
    default:
      break;
  }
  return 255;
}
void Object::update(uint8_t groundPixels[16]){
  switch (objId){
    case 1: // Chest
      break;
    case 2: // Barrel
      break;
    case 3: // Priest
      break;
    case 4: // Lab Globe
      break;
    case 5: // Teardrop
    {
      uint8_t pixelPosY = toPixel(tileY,offsY);
      pixelPosY += random(1,3);
      tileY = toTile(pixelPosY);
      offsY = toOffs(pixelPosY);
      if(pixelPosY + 8 > groundPixels[tileX]){
        tileY = 0;
        offsY = 0;
        tileX = random(2,14); 
      }
      break;
    }
    case 6: // Chain Platform
    {
      if(tileX == status){
        if(status == 2){
          status = 13;
        }else{
          status = 2;
        }
      }
      uint8_t pixelPosX = toPixel(tileX,offsX);
      uint8_t deltaPos = random(-1,1);
      //if(millis()-animLastTime > 200){
      if(status == 2){
        deltaPos -= 2;
      }else{
        deltaPos += 2;
      }
      pixelPosX += deltaPos;
      tileX = toTile(pixelPosX);
      offsX = toOffs(pixelPosX);
      break;
    }
    case 7: // Lever
      break;
    case 8: // Drain Entrance
      break;
    case 9: // Telescope
      break;
    case 10: // Turkey Dish
      break;
    default:
      break;
  }
  return;
}
