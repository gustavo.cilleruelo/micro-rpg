class Player: public Character{
  public:
    uint8_t items[4] = {0,0,0,0};//{51,53,66,54};
    uint8_t maxStamina = 3;
    uint8_t stamina = 3;
    uint8_t maxLives = 3;
    uint8_t isAttacking = 0; // 0: no, 1: yes
    Projectile fireball = Projectile(0,0,0,0,0);
    short jumpInterval = 500;
    long jumpLastTime;
    int attackLastTime = 0;
    byte canInteract = 0;
    Item mapItem = Item(0,0,0,0,0);
    Player(uint8_t tx, uint8_t ty, uint8_t ox, uint8_t oy):Character(tx, ty, ox, oy){
      lives = 3;
      sprite = 0;
      animLastTime = millis();
    }
    void updatePlayer(uint8_t (&groundPixels)[16], uint8_t (&roofPixels)[16], Object* cObj[3], Character* npc);
    void drawPlayer();
    void drawDashboard();
    void addItem(uint8_t loot);
    void drawItems(uint8_t x, uint8_t y);
    void drawHUD();
    void getHitBy(uint8_t att);
    //void updatePosition(uint8_t (&groundPixels)[16], uint8_t (&roofPixels)[16], Object* cObj[3] );
  private:
    void animationEngine();
    //void updatePosition(uint8_t (&groundPixels)[16], uint8_t (&roofPixels)[16], Object* cObj[3] );
    void updateStamina();
    void getUpdate();
    void itemEffect(uint8_t item);
    void cancelEffect(uint8_t item);
    uint8_t checkInteract(Object* cObj[3], Character* npc);
};

void Player::getUpdate(){ 

  // X AXIS MOMENTUM /////////////////////////////////////////
  if(digitalRead(buttRight) == LOW){ // right
    dir = 1;
    if(vX < 3){
      vX += 1;
    }
  }else if(digitalRead(buttLeft) == LOW){ // left
    dir = 0;
    if(vX > -4){
      vX -= 1;
    }
  }else if(vX < 0){
    vX += 1;
  }else if(vX > 0){
    vX -= 1;
  }
  // Y AXIS MOMENTUM /////////////////////////////////////////
  if(digitalRead(buttJump) == LOW && millis()-jumpLastTime > 300){
    if(stamina > 0){
      jumpLastTime = millis();
      if(vY > 0){
          vY = 0;
      }
      vY -= 4;
      stamina -= 1;
    }
  }
  // gravity
  vY += 1;
}
void Player::animationEngine(){
  if(isAttacking == 1){
    switch(items[1]){
      case 53: // sword
      {
        if(dir == 1){
          sprite = 25;
        } else {
          sprite = 27;
        }      
        break;
      }
      case 70:
      {
        if(dir == 1){
          sprite = 29;
        } else {
          sprite = 31;
        }      
        break;
      }
      case 71:
      {
        if(dir == 1){
          sprite = 41;
        } else {
          sprite = 43;
        }      
        break;
      }      
      default:
        break;
    }
  }
  else if(vY == 0 && vX == 0){
    if(millis() - animLastTime > 1500){ //animation
      animLastTime = millis(); // IDLE
      if(dir == 1){
        if(sprite == 1){
          sprite = 0;
        }else{
          sprite = 1;
        } 
      }else if(dir == 0){
        if(sprite == 11){
          sprite = 10;
        }else{
          sprite = 11;
        } 
      } 
    }else if(sprite != 0 && sprite != 10){ //if not in idle animation
      if(dir == 1){
        sprite = 1;
      }else if(dir == 0){
        sprite = 11; 
      }       
    } 
  }else if(vY == 0 && vX != 0 && millis() - animLastTime > abs(vX)*(800-abs(vX)*200)){
      animLastTime = millis();
      if(vX > 0){// RUN RIGHT
        if(sprite == 6){
          sprite = 7;
        }else{
          sprite = 6;
        }  
      }else if(vX < 0){// RUN LEFT
        if(sprite == 8){
          sprite = 9;
        }else{
          sprite = 8;
        }  
      }   
  }else{
    //animLastTime = millis(); // JUMP
    if(vY == 0){
      if(dir == 1){
        sprite = 1;
      }else if(dir == 0){
        sprite = 11; 
      }  
    }else if(vY < 0){ // UP
      if(vX > 0){
        sprite = 2;
      } else if(vX < 0){
        sprite = 3;
      } 
    }else{ // DOWN
      if(vX > 0){
        sprite = 4;
      } else if(vX < 0){
        sprite = 5;
      } else {
        if(dir == 1){
          sprite = 1;
        }else if(dir == 0){
          sprite = 11; 
        }  
      }  
    }
  }
}
void Player::updateStamina(){
 // stamina
  if(stamina < maxStamina && millis()-jumpLastTime > jumpInterval){
    jumpLastTime = millis();
    ++stamina;
  }
}
void Player::updatePlayer(uint8_t (&groundPixels)[16], uint8_t (&roofPixels)[16], Object* cObj[3], Character* npc ){
  updatePosition(groundPixels, roofPixels, cObj);
  uint8_t yPos = toPixel(tileY,offsY) + 8;
  if(yPos > 63 ){
    --lives;
    tileX = 0;
    offsX = -2;
  }
  updateStamina();
  checkInteract(cObj, npc);
  animationEngine();
  if(mapItem.sprite != 0){
    mapItem.updatePosition(groundPixels, roofPixels, cObj);
  }
  if(fireball.firedBy != 0){
    fireball.updatePosition(groundPixels, roofPixels, cObj);
  }
}
void Player::drawPlayer(){
  spriteRenderer(tileX,tileY,offsX,offsY,sprite);
    // exclamation to signal interactions
  if(canInteract == 1){
    spriteRenderer(tileX,tileY-1,offsX,offsY,69);
  }
  else if(isAttacking == 1){
    switch(items[1]){
      case 53: // sword
      {
        if(dir == 1){
          spriteRenderer(tileX+1,tileY,offsX,offsY,26);
        } else {
          spriteRenderer(tileX-1,tileY,offsX,offsY,28);
        }      
        break;
      }
      case 70: // staff
      {
        if(dir == 1){
          spriteRenderer(tileX+1,tileY,offsX,offsY,30);
          spriteRenderer(tileX+1,tileY-1,offsX,offsY,33);
        } else {
          spriteRenderer(tileX-1,tileY,offsX,offsY,32);
          spriteRenderer(tileX-1,tileY-1,offsX,offsY,34);
        }      
        break;
      }
      case 71: // bow
      {
        if(dir == 1){
          spriteRenderer(tileX+1,tileY,offsX,offsY,42);
        } else {
          spriteRenderer(tileX-1,tileY,offsX,offsY,44);
        }      
        break;
      }
      default:
        break;
    }
  }
  // items on the map
  if(mapItem.sprite != 0){
    spriteRenderer(mapItem.tileX,mapItem.tileY,mapItem.offsX,mapItem.offsY,mapItem.sprite);
  }
  if(fireball.firedBy != 0){
    spriteRenderer(fireball.tileX,fireball.tileY,fireball.offsX,fireball.offsY,fireball.getSprite());
  }
}
void Player::drawItems(uint8_t x, uint8_t y){
  /*SC.drawFrame(x,y,28,10);
  x = x + 1;
  y = y + 1;
  for(int i = 0; i < 3; ++i){
    if(items[i] != 0){
      spriteRenderer(x/8,y/8,x%8,y%8,items[i]);
      x = x + 9;
    }
  }*/
}
void Player::drawDashboard(){
  int x = 31;
  int y = 59; // 3
  for(int i = 0; i < stamina; ++i){
    SC.drawDisc(x,y,2);
    x += 6;
  }
  for(int j = stamina; j < maxStamina; ++j){
    SC.drawCircle(x,y,2);
    x += 6;
  }
  x = 8;
  y = 7; // 0
  for(int i = 0; i < lives; ++i){
    spriteRenderer(x,y,0,0,12);
    x += 1;
  }
  for(int j = lives; j < maxLives; ++j){
    spriteRenderer(x,y,0,0,13);
    x += 1;
  }
  //drawItems(0,0);
}
void Player::addItem(uint8_t loot){
  if(items[itemType(loot)] != 0){
    cancelEffect(items[itemType(loot)]);
  }
  items[itemType(loot)] = loot;
  itemEffect(loot);
}
void Player::itemEffect(uint8_t item){   
  switch(item){
    case 68: //scroll
      maxStamina += 1;
      stamina += 1;
      break;
    case 51:
      if(lives < maxLives){
        lives += 1;
      }
      break;
    default:
      break;
  }
}
void Player::cancelEffect(uint8_t item){
  switch(item){
    case 68: //scroll
      maxStamina -= 1;
      break;
    default:
      break;
  }
}
uint8_t Player::checkInteract(Object* cObj[3], Character* npc){ //cItems
  // interaction with objects and picking up items
  // OBJECTS
  canInteract = 0;
  for(uint8_t i = 0; i < 3; ++i){
    if(cObj[i]->loot != 255 && abs(cObj[i]->tileX - tileX) < 2 && abs(cObj[i]->tileY - tileY) < 2){
      if(digitalRead(buttAction) == LOW){
        uint8_t dropItem = cObj[i]->interact();
        if(dropItem != 0){ // drops an item
          mapItem = Item(dropItem,cObj[i]->tileX, cObj[i]->tileY-1,0,cObj[i]->offsY);
        }
      }
      canInteract = 1;
      break;
    }
  }
  // perform attack if no interaction possible
  //debugPrint(canInteract,10,10);
  isAttacking = 0;
  if(digitalRead(buttAction) == LOW && canInteract == 0){
      switch(items[1]){ //depending on the weapon
        case 53: //sword
          isAttacking = 1;
          if((dir == 1 && rectangleCollision(toPixel(tileX,offsX),toPixel(tileY,offsY),8,8,toPixel(npc->tileX,npc->offsX),toPixel(npc->tileY,npc->offsY),8,8)==1)
          |(dir == 0 && rectangleCollision(toPixel(tileX-1,offsX),toPixel(tileY,offsY),8,8,toPixel(npc->tileX,npc->offsX),toPixel(npc->tileY,npc->offsY),8,8)==1)){
            npc->getHitBy(53);
          }
          break;
        case 70: // staff
        {
          if(millis()/1000 - attackLastTime > 1){
            isAttacking = 1;
            attackLastTime = millis()/1000;
            fireball.firedBy = 70;
            fireball.offsX = offsX;
            fireball.tileY = tileY;
            fireball.offsY = offsY;
            fireball.vY = 0;
            if(dir == 0){
              fireball.tileX = tileX - 1;
              fireball.vX = -4;
            }else{
              fireball.tileX = tileX + 1;
              fireball.vX = 4;
            }
            break;
          }
        }
        case 71: // bow
        {
          if(millis()/1000 - attackLastTime > 1){
            isAttacking = 1;
            attackLastTime = millis()/1000;
            fireball.firedBy = 71;
            fireball.offsX = offsX;
            fireball.tileY = tileY;
            fireball.offsY = offsY;
            fireball.vY = 0;
            if(dir == 0){
              fireball.tileX = tileX - 1;
              fireball.vX = -4;
            }else{
              fireball.tileX = tileX + 1;
              fireball.vX = 4;
            }
            break;
          }  
        }        
        default:
          break;
      }
    
  }
  if(fireball.firedBy != 0){
    if(fireball.tileX == npc->tileX && fireball.tileY == npc->tileY){
      npc->getHitBy(fireball.firedBy);
      fireball.firedBy = 0;
    }
  }
  
  // ITEMS
  if(mapItem.sprite != 0 && mapItem.tileX == tileX && mapItem.tileY == tileY && mapItem.vX == 0){
    addItem(mapItem.sprite);
    mapItem.sprite = 0;
  }
}
void Player::drawHUD(){
  // HUD frame
  SC.drawFrame(31,15,64,32);
  // HUD player
  spriteRenderer(toTile(43),toTile(28),toOffs(43),toOffs(28),sprite);
  // Lives and Stamina
  int x = 70;
  int y = 20; // 3
  for(int i = 0; i < stamina; ++i){
    SC.drawDisc(x,y,2);
    x += 6;
  }
  for(int j = stamina; j < maxStamina; ++j){
    SC.drawCircle(x,y,2);
    x += 6;
  }
  x = 8;
  y = 3; // 0
  for(int i = 0; i < lives; ++i){
    spriteRenderer(x,y,0,0,12);
    x += 1;
  }
  for(int j = lives; j < maxLives; ++j){
    spriteRenderer(x,y,0,0,13);
    x += 1;
  }  
  // gameTime
  SC.setCursor(68,42);
  SC.print(gameTime/1000);
  // helmet 3
  if(items[3] == 0){
    SC.drawFrame(43,17,8,8);
  } else {
    spriteRenderer(toTile(43),toTile(17),toOffs(43),toOffs(16),items[3]);
  } 
  // utility 0
  if(items[0] == 0){
    SC.drawFrame(33,27,8,8);
  } else {
    spriteRenderer(toTile(33),toTile(27),toOffs(33),toOffs(27),items[0]);
  }
  // weapons 1
  if(items[1] == 0){
    SC.drawFrame(53,27,8,8);
  } else {
    spriteRenderer(toTile(53),toTile(27),toOffs(53),toOffs(27),items[1]);
  }
  // boots 2
  if(items[2] == 0){
    SC.drawFrame(43,37,8,8);
  } else {
    spriteRenderer(toTile(43),toTile(38),toOffs(43),toOffs(38),items[2]);
  }
}
void Player::getHitBy(uint8_t att){
  if(att == 117 or att == 118){ // tear
    lives -= 1;
  }
  switch(att){
    case 1: // Skelly
      lives -= 1;
      break;
    case 2: // Bat
      break;
    case 3: // Eye
      break;
    case 4: // Knight
      lives -= 1;
      break;
    case 5: // Snake
      break;
    default:
      break;
  }
}
