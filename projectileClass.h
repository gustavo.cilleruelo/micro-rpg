class Projectile: public Character{
  public:
  uint8_t firedBy;
    Projectile(uint8_t origin, uint8_t tx, uint8_t ty, uint8_t ox, uint8_t oy):Character(tx, ty, ox, oy){
      firedBy = origin;
      sprite = 35;
      animLastTime = millis();
      dir = 1; // 0: left, 1: right
      lives = 1;
      vX = 3;
      vY = 0;
    } 
    uint8_t getSprite();
    //void update(uint8_t (&groundPixels)[16]);
    void getUpdate();
    //void getHitBy(uint8_t att);
  private:
    void setSprite(uint8_t spriteLeft1, uint8_t spriteLeft2, uint8_t spriteRight1, uint8_t spriteRight2);
};

void Projectile::setSprite(uint8_t spriteRight1, uint8_t spriteRight2, uint8_t spriteLeft1, uint8_t spriteLeft2){
  if(vX > 0){
    if(sprite == spriteRight1){
      sprite = spriteRight2;
    }else{
      sprite = spriteRight1;
    }
  }else if(vX < 0){
    if(sprite == spriteLeft1){
      sprite = spriteLeft2;
    }else{
      sprite = spriteLeft1;
    }
  }
  return;
}

uint8_t Projectile::getSprite() {
  if(sprite == 0){ // DED
    return 0;
  }
  switch(firedBy) {
    case 70: // staff ->  fireball
      if(millis() - animLastTime > 100){
        setSprite(35,36,37,38);
      }
      break;
    case 71: // bow -> arrow
      setSprite(40,40,39,39);  
      break;
    default:
      break;
  }
  return sprite;
}

void Projectile::getUpdate(){
  if(vX == 0 || tileX == 14){
    firedBy = 0;
  }
  vY += 1;
}