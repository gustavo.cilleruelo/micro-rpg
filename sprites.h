const byte PROGMEM idleRight1_bits[] = {
   0x00, 0x3c, 0x3c, 0x14, 0x3c, 0x3c, 0x3c, 0x24 };
const byte PROGMEM brickWall_bits[] = {
   0x40, 0xff, 0x08, 0x08, 0x08, 0xff, 0x40, 0x40 };
const byte PROGMEM rockFloor_bits[] = {
   0xff, 0x3c, 0x12, 0x13, 0xf1, 0x39, 0x1f, 0xe3 };
const byte PROGMEM idleRight2_bits[] = {
   0x3c, 0x3c, 0x14, 0x3c, 0x3c, 0x3c, 0x3c, 0x24 };
const byte PROGMEM jumpRight_bits[] = {
   0x3c, 0x14, 0x3c, 0x3c, 0x3c, 0x3c, 0x3c, 0x12 };
const byte PROGMEM jumpLeft_bits[] = {
   0x3c, 0x28, 0x3c, 0x3c, 0x3c, 0x3c, 0x3c, 0x48 };
const byte PROGMEM fallRight_bits[] = {
   0x3c, 0x3c, 0x3c, 0x14, 0x3c, 0x3c, 0x3c, 0x48 };
const byte PROGMEM fallLeft_bits[] = {
   0x3c, 0x3c, 0x3c, 0x28, 0x3c, 0x3c, 0x3c, 0x12 }; 
const byte PROGMEM runRight1_bits[] = {
   0x3c, 0x3c, 0x14, 0x3c, 0x3c, 0x3c, 0x3c, 0x04 };
const byte PROGMEM runRight2_bits[] = {
   0x3c, 0x3c, 0x14, 0x3c, 0x3c, 0x3c, 0x3c, 0x20 };
const byte PROGMEM runLeft1_bits[] = {
   0x3c, 0x3c, 0x28, 0x3c, 0x3c, 0x3c, 0x3c, 0x04 };
const byte PROGMEM runLeft2_bits[] = {
   0x3c, 0x3c, 0x28, 0x3c, 0x3c, 0x3c, 0x3c, 0x20 };
const byte PROGMEM idleLeft1_bits[] = {
   0x00, 0x3c, 0x3c, 0x28, 0x3c, 0x3c, 0x3c, 0x24 };
const byte PROGMEM idleLeft2_bits[] = {
   0x3c, 0x3c, 0x28, 0x3c, 0x3c, 0x3c, 0x3c, 0x24 };
const byte PROGMEM potionItem1_bits[] = {
   0x00, 0x3c, 0x18, 0x18, 0x34, 0x62, 0x62, 0x3c };
const byte PROGMEM keyItem1_bits[] = {
   0x00, 0x00, 0x00, 0x07, 0x7d, 0x67, 0x00, 0x00 };
const byte PROGMEM swordItem1_bits[] = {
   0x08, 0x18, 0x18, 0x18, 0x18, 0x3c, 0x18, 0x18 };
const byte PROGMEM helmetItem1_bits[] = {
   0x00, 0x3c, 0x7e, 0x7e, 0x42, 0x6e, 0x6e, 0x6e };
const byte PROGMEM chainBottom1_bits[] = {
   0x1c, 0x14, 0x14, 0x1c, 0x08, 0x1c, 0x14, 0x3e };
const byte PROGMEM chainMiddle1_bits[] = {
   0x14, 0x1c, 0x08, 0x1c, 0x14, 0x14, 0x1c, 0x08 };
const byte PROGMEM chainTop1_bits[] = {
   0x3e, 0x1c, 0x14, 0x14, 0x1c, 0x08, 0x1c, 0x14 };
const byte PROGMEM water1_bits[] = {
   0x00, 0x00, 0x00, 0x00, 0x22, 0x77, 0xff, 0xff };
const byte PROGMEM fullHeart_bits[] = {
   0x66, 0xdb, 0xa5, 0xbd, 0xbd, 0x5a, 0x24, 0x18 };
const byte PROGMEM  emptyHeart_bits[] = {
   0x66, 0xdb, 0x81, 0x81, 0x81, 0x42, 0x24, 0x18 };
/*const byte PROGMEM wallTorch1_bits[] = {
   0x08, 0x18, 0x34, 0x24, 0x3c, 0x3c, 0x3c, 0x18 };
const byte PROGMEM wallTorch2_bits[] = {
   0x00, 0x08, 0x14, 0x24, 0x3c, 0x3c, 0x3c, 0x18 };
const byte PROGMEM wallTorch3_bits[] = {
   0x00, 0x30, 0x2c, 0x34, 0x3c, 0x3c, 0x3c, 0x18 };
const byte PROGMEM wallTorch4_bits[] = {
   0x10, 0x18, 0x2c, 0x24, 0x3c, 0x3c, 0x3c, 0x18 };
const byte PROGMEM wallTorch5_bits[] = {
   0x00, 0x18, 0x14, 0x24, 0x3c, 0x3c, 0x3c, 0x18 };*/
const byte PROGMEM chestLeftClosed_bits[] = {
   0x00, 0x00, 0x7e, 0x81, 0xff, 0x89, 0x81, 0xff };
const byte PROGMEM chestLeftOpen_bits[] = {
   0x3e, 0x42, 0x84, 0x84, 0xff, 0x89, 0x81, 0xff };
const byte PROGMEM skellyIdleRight0_bits[] = {
   0x00, 0x3c, 0x14, 0x3c, 0x1c, 0x08, 0x1c, 0x14 };
const byte PROGMEM skellyIdleRight1_bits[] = {
   0x3c, 0x14, 0x3c, 0x08, 0x1c, 0x08, 0x1c, 0x14 };
const byte PROGMEM skellyIdleLeft0_bits[] = {
   0x00, 0x3c, 0x28, 0x3c, 0x38, 0x10, 0x38, 0x28 };
const byte PROGMEM skellyIdleLeft1_bits[] = {
   0x3c, 0x28, 0x3c, 0x10, 0x38, 0x10, 0x38, 0x28 };
const byte PROGMEM skellyRunLeft0_bits[] = {
   0x3c, 0x28, 0x3c, 0x10, 0x38, 0x10, 0x38, 0x20 };
const byte PROGMEM skellyRunLeft1_bits[] = {
   0x3c, 0x28, 0x3c, 0x10, 0x38, 0x10, 0x38, 0x08 };
const byte PROGMEM skellyRunRight0_bits[] = {
   0x3c, 0x14, 0x3c, 0x08, 0x1c, 0x08, 0x1c, 0x04 };
const byte PROGMEM skellyRunRight1_bits[] = {
   0x3c, 0x14, 0x3c, 0x08, 0x1c, 0x08, 0x1c, 0x10 };
const byte PROGMEM boots1_bits[] = {
   0x00, 0x00, 0x66, 0x00, 0x66, 0x66, 0xee, 0x00 };
const byte PROGMEM jar_bits[] = {
   0x00, 0x3c, 0x18, 0xdb, 0xbd, 0x7e, 0x3c, 0x18 };
const byte PROGMEM exclamation_bits[] = {
   0x00, 0x18, 0x18, 0x18, 0x18, 0x00, 0x18, 0x00 };
const byte PROGMEM scroll_bits[] = {
   0x7c, 0x82, 0x42, 0x5a, 0x42, 0x5a, 0x41, 0x3e };
const byte PROGMEM priestIdle0_bits[] = {
   0x04, 0x0e, 0xf4, 0xa4, 0xf4, 0xfc, 0xf4, 0x94 };
const byte PROGMEM priestIdle1_bits[] = {
   0x02, 0xf7, 0xa2, 0xf2, 0xf6, 0xfa, 0xf2, 0x92 };
const byte PROGMEM sword1AttackRight_pl_bits[] = {
   0x3c, 0x3c, 0x14, 0xbc, 0xfc, 0xbc, 0x3c, 0x24 };
const byte PROGMEM sword1AttackRight_sw_bits[] = {
   0x00, 0x00, 0x00, 0x00, 0x0f, 0x00, 0x00, 0x00 };
const byte PROGMEM sword1AttackLeft_pl_bits[] = {
   0x3c, 0x3c, 0x28, 0x3d, 0x3f, 0x3d, 0x3c, 0x24 };
const byte PROGMEM sword1AttackLeft_sw_bits[] = {
   0x00, 0x00, 0x00, 0x00, 0xf0, 0x00, 0x00, 0x00 };
const byte PROGMEM staff1_bits[] = {
   0xa0, 0x20, 0xe0, 0x10, 0x08, 0x04, 0x02, 0x01 };
const byte PROGMEM staff1AttackRight_pl_bits[] = {
   0x3c, 0x14, 0x3c, 0x3c, 0xfc, 0x7c, 0x3c, 0x24 };
const byte PROGMEM staff1AttackRight_sw_bits[] = {
   0x38, 0x04, 0x02, 0x01, 0x00, 0x00, 0x00, 0x00 };
const byte PROGMEM staff1AttackLeft_pl_bits[] = {
   0x3c, 0x28, 0x3c, 0x3c, 0x3f, 0x3e, 0x3c, 0x24 };
const byte PROGMEM staff1AttackLeft_sw_bits[] = {
   0x1c, 0x20, 0x40, 0x80, 0x00, 0x00, 0x00, 0x00 };
const byte PROGMEM staff1AttackTopRight_sw_bits[] = {
   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x28, 0x08 };
const byte PROGMEM staff1AttackTopLeft_sw_bits[] = {
   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x14, 0x10 }; 
const byte PROGMEM fireballRight1_bits[] = {
   0x00, 0x00, 0x30, 0x7c, 0x78, 0x30, 0x00, 0x00 };  
const byte PROGMEM  fireballRight2_bits[] = {
   0x00, 0x00, 0x30, 0x78, 0x7c, 0x38, 0x00, 0x00 };
const byte PROGMEM  fireballLeft1_bits[] = {
   0x00, 0x00, 0x0c, 0x3e, 0x1e, 0x0c, 0x00, 0x00 };
const byte PROGMEM fireballLeft2_bits[] = {
   0x00, 0x00, 0x0c, 0x1e, 0x3e, 0x1c, 0x00, 0x00 };
const byte PROGMEM arrowRight_bits[] = {
   0x00, 0x00, 0x20, 0x7e, 0x20, 0x00, 0x00, 0x00 };
const byte PROGMEM arrowLeft_bits[] = {
   0x00, 0x00, 0x04, 0x7e, 0x04, 0x00, 0x00, 0x00 };
const byte PROGMEM bowAttackRight_pl_bits[] = {
   0x3c, 0x14, 0x3c, 0xfc, 0x3c, 0x3c, 0x3c, 0x24 };
const byte PROGMEM bowAttackRight_sw_bits[] = {
   0x01, 0x02, 0x02, 0x02, 0x02, 0x02, 0x01, 0x00 };
const byte PROGMEM bowAttackLeft_pl_bits[] = {
   0x3c, 0x28, 0x3c, 0x3f, 0x3c, 0x3c, 0x3c, 0x24 };
const byte PROGMEM bowAttackLeft_sw_bits[] = {
   0x80, 0x40, 0x40, 0x40, 0x40, 0x40, 0x80, 0x00 };
const byte PROGMEM bow_bits[] = {
   0x00, 0x5e, 0x20, 0x50, 0x48, 0x44, 0x40, 0x00 };
const byte PROGMEM batFlyRight1_bits[] = {
   0x00, 0x14, 0xdd, 0x6a, 0x7e, 0x3c, 0x18, 0x00 };
const byte PROGMEM batFlyRight2_bits[] = {
   0x00, 0x14, 0x1c, 0xeb, 0x7e, 0x3c, 0x18, 0x00 };  
const byte PROGMEM batFlyLeft1_bits[] = {
   0x00, 0x28, 0xbb, 0x56, 0x7e, 0x3c, 0x18, 0x00 };
const byte PROGMEM batFlyLeft2_bits[] = {
   0x00, 0x28, 0x38, 0xd7, 0x7e, 0x3c, 0x18, 0x00 };
const byte PROGMEM eyeCenter1_bits[] = {
   0x00, 0xe0, 0x18, 0x84, 0xc2, 0x84, 0x18, 0xe0 };
const byte PROGMEM eyeCenter2_bits[] = {
   0x00, 0x07, 0x18, 0x21, 0x43, 0x21, 0x18, 0x07 };
const byte PROGMEM eyeLeft1_bits[] = {
   0x00, 0xe0, 0x18, 0xc4, 0xe2, 0xc4, 0x18, 0xe0 };
const byte PROGMEM eyeLeft2_bits[] = {
   0x00, 0x07, 0x18, 0x20, 0x41, 0x20, 0x18, 0x07 };
const byte PROGMEM eyeRight1_bits[] = {
   0x00, 0xe0, 0x18, 0x04, 0x82, 0x04, 0x18, 0xe0 };
const byte PROGMEM eyeRight2_bits[] = {
   0x00, 0x07, 0x18, 0x23, 0x47, 0x23, 0x18, 0x07 };
const byte PROGMEM tearDropLeft1_bits[] = {
   0x08, 0x1c, 0x1e, 0x3f, 0x7d, 0x7b, 0x3e, 0x1c }; 
const byte PROGMEM tearDropRight1_bits[] = {
   0x10, 0x38, 0x78, 0xfc, 0xbe, 0xde, 0x7c, 0x38 };  
const byte PROGMEM barrel_bits[] = {
   0x7c, 0xba, 0xaa, 0xaa, 0xaa, 0xaa, 0xaa, 0x7c };
const byte PROGMEM banner_bits[] = {
   0xff, 0x81, 0xbd, 0xa5, 0xa5, 0xa5, 0xa5, 0xa5, 0xa5, 0xa5, 0xa5, 0xa5,
   0xa5, 0xa5, 0xa5, 0xa5, 0xa5, 0xbd, 0xa5, 0x81, 0x99, 0xa5, 0xc3, 0x81 };
const byte PROGMEM knightIdleRight0_bits[] = {
   0x00, 0x1e, 0x02, 0x97, 0x4d, 0x2d, 0x1f, 0x0a };
const byte PROGMEM knightIdleRight1_bits[] = {
   0x1e, 0x02, 0x96, 0x4f, 0x2d, 0x1d, 0x0f, 0x0a };
const byte PROGMEM knightIdleLeft0_bits[] = {
   0x00, 0x78, 0x40, 0xe9, 0xb2, 0xb4, 0xf8, 0x50 };
const byte PROGMEM knightIdleLeft1_bits[] = {
   0x78, 0x40, 0x69, 0xf2, 0xb4, 0xb8, 0xf0, 0x50 };
const byte PROGMEM platformIdle0_bits[] = {
   0xff, 0xff, 0x7e, 0x1e, 0x0c, 0x08, 0x04, 0x08, 0x14, 0x14, 0x08, 0x10,
   0x08, 0x14, 0x14, 0x08 };
const byte PROGMEM platformIdle1_bits[] = {
   0xff, 0xff, 0x7e, 0x1e, 0x0c, 0x08, 0x10, 0x08, 0x14, 0x14, 0x08, 0x04,
   0x08, 0x14, 0x14, 0x08 };
const byte PROGMEM platformLeft_bits[] = {
   0xff, 0xff, 0x7e, 0x1e, 0x0c, 0x08, 0x04, 0x08, 0x14, 0x14, 0x08, 0x10,
   0x20, 0x50, 0x50, 0x20 };    
const byte PROGMEM platformRight_bits[] = {
   0xff, 0xff, 0x7e, 0x1e, 0x0c, 0x08, 0x10, 0x08, 0x14, 0x14, 0x08, 0x04,
   0x02, 0x05, 0x05, 0x02 };     
const byte PROGMEM leverOn_bits[] = {
   0x00, 0x00, 0xc0, 0xc0, 0xe0, 0xd0, 0x0c, 0x0c };
const byte PROGMEM leverOff_bits[] = {
   0x0c, 0x0c, 0xd0, 0xe0, 0xc0, 0xc0, 0x00, 0x00 };   
const byte PROGMEM drainEntranceClosed_bits[] = {
   0xf8, 0x01, 0x04, 0x02, 0xf2, 0x04, 0xd9, 0x09, 0xd5, 0x0a, 0xd5, 0x0a,
   0xb5, 0x0a, 0xd5, 0x0a, 0xd9, 0x09, 0xf2, 0x04 };
const byte PROGMEM drainEntranceOpen_bits[] = {
   0xf8, 0x01, 0x04, 0x02, 0xf2, 0x04, 0x99, 0x09, 0xed, 0x0b, 0xfd, 0x0b,
   0xfd, 0x0a, 0x7d, 0x0b, 0xf9, 0x09, 0xf2, 0x04 };
const byte PROGMEM windowEmpty_bits[] = {
   0xe0, 0x07, 0xf8, 0x1f, 0x1c, 0x38, 0x06, 0x60, 0x16, 0x60, 0x0b, 0xc0,
   0x0b, 0xc0, 0x03, 0xd0, 0x03, 0xd0, 0x03, 0xd0, 0x03, 0xc8, 0x06, 0x66,
   0x06, 0x60, 0x1c, 0x38, 0xf8, 0x1f, 0xe0, 0x07 };
const byte PROGMEM windowStars_bits[] = {
   0xe0, 0x07, 0xf8, 0x1f, 0x1c, 0x38, 0x46, 0x62, 0x16, 0x67, 0x3b, 0xc2,
   0x7f, 0xc8, 0x3b, 0xc0, 0x93, 0xe4, 0x07, 0xce, 0x23, 0xdf, 0x76, 0x6e,
   0x26, 0x64, 0x1c, 0x39, 0xf8, 0x1f, 0xe0, 0x07 };
const byte PROGMEM telescopeClosed_bits[] = {
   0x00, 0x06, 0x90, 0x0f, 0xe0, 0x0f, 0x78, 0x06, 0x9e, 0x01, 0x67, 0x00,
   0x19, 0x00, 0x46, 0x00, 0x00, 0x00, 0xe0, 0x00, 0x58, 0x03 };
const byte PROGMEM telescopeOpen_bits[] = {
   0x00, 0x06, 0x90, 0x09, 0xe0, 0x09, 0x78, 0x06, 0x9e, 0x01, 0x67, 0x00,
   0x19, 0x00, 0x46, 0x00, 0x00, 0x00, 0xe0, 0x00, 0x58, 0x03 };
const byte PROGMEM globe1_bits[] = {
   0x70, 0x01, 0xd8, 0x00, 0x24, 0x01, 0x4d, 0x01, 0xcd, 0x01, 0xba, 0x00,
   0x74, 0x00, 0x0a, 0x00, 0x30, 0x00, 0x20, 0x00, 0xf8, 0x00 };
const byte PROGMEM globe2_bits[] = {
   0x70, 0x01, 0xc8, 0x00, 0x9c, 0x01, 0x9d, 0x01, 0x65, 0x01, 0xda, 0x00,
   0x74, 0x00, 0x0a, 0x00, 0x30, 0x00, 0x20, 0x00, 0xf8, 0x00 };
const byte PROGMEM globe3_bits[] = {
   0x70, 0x01, 0xf8, 0x00, 0x9c, 0x01, 0x75, 0x01, 0xcd, 0x01, 0xda, 0x00,
   0x74, 0x00, 0x0a, 0x00, 0x30, 0x00, 0x20, 0x00, 0xf8, 0x00 };
const byte PROGMEM turkey_bits[] = {
   0x40, 0xd8, 0x3c, 0x7e, 0x3c, 0x81, 0xff, 0x7e };
const byte PROGMEM turkeyEaten_bits[] = {
   0x40, 0xc0, 0x20, 0x32, 0x1c, 0x81, 0xff, 0x7e };
const byte PROGMEM chairLeft_bits[] = {
   0x60, 0x50, 0x50, 0x50, 0x50, 0x7c, 0x44, 0x44 };
const byte PROGMEM chairRight_bits[] = {
   0x06, 0x0a, 0x0a, 0x0a, 0x0a, 0x3e, 0x22, 0x22 };
const byte PROGMEM snakeIdleLeft0_bits[] = {
   0x00, 0x00, 0x0c, 0x1e, 0x18, 0x2c, 0x46, 0x3c };
const byte PROGMEM snakeIdleLeft1_bits[] = {
   0x00, 0x0c, 0x1e, 0x10, 0x18, 0x2c, 0x46, 0x3c };
const byte PROGMEM snakeIdleRight0_bits[] = {
   0x00, 0x00, 0x30, 0x78, 0x18, 0x34, 0x62, 0x3c };
const byte PROGMEM snakeIdleRight1_bits[] = {
   0x00, 0x30, 0x78, 0x08, 0x18, 0x34, 0x62, 0x3c };
const byte PROGMEM snakeRunLeft2_bits[] = {
   0x00, 0x06, 0x0f, 0x08, 0x0c, 0x46, 0x43, 0x3e };
const byte PROGMEM snakeRunLeft3_bits[] = {
   0x00, 0x06, 0x0f, 0x08, 0x0c, 0x86, 0x43, 0x3e };
const byte PROGMEM snakeRunLeft4_bits[] = {
   0x00, 0x0c, 0x1e, 0x10, 0x18, 0x4c, 0x46, 0x3c };
const byte PROGMEM snakeRunRight2_bits[] = {
   0x00, 0x60, 0xf0, 0x10, 0x30, 0x62, 0xc2, 0x7c };
const byte PROGMEM snakeRunRight3_bits[] = {
   0x00, 0x60, 0xf0, 0x10, 0x30, 0x61, 0xc2, 0x7c };
const byte PROGMEM snakeRunRight4_bits[] = {
   0x00, 0x30, 0x78, 0x08, 0x18, 0x32, 0x62, 0x3c };
const byte PROGMEM flowers0_bits[] = {
   0x08, 0x00, 0x14, 0x00, 0x08, 0x01, 0x92, 0x02, 0x25, 0x01, 0xaa, 0x00,
   0x50, 0x00, 0x20, 0x00 };
const byte PROGMEM flowers1_bits[] = {
   0x20, 0x00, 0x50, 0x00, 0x24, 0x00, 0x0a, 0x01, 0xa4, 0x02, 0x28, 0x01,
   0xd0, 0x00, 0x20, 0x00 };
const byte PROGMEM flowers2_bits[] = {
   0x80, 0x00, 0x40, 0x01, 0x84, 0x00, 0x4a, 0x00, 0x24, 0x01, 0xa8, 0x02,
   0x50, 0x01, 0x20, 0x00 };
const byte PROGMEM skellyBone_bits[] = {
   0x00, 0x20, 0x60, 0x10, 0x08, 0x06, 0x04, 0x00 };
const byte PROGMEM knightLeftAttack1_bits[] = {
   0x79, 0x42, 0x6c, 0xf8, 0xb0, 0xb0, 0xf0, 0x50 };
const byte PROGMEM knightLeftAttack2_bits[] = {
   0x78, 0x40, 0x68, 0xf3, 0xbc, 0xb0, 0xf0, 0x50 };
const byte PROGMEM knightLeftAttack3_bits[] = {
   0x79, 0x42, 0x6c, 0xf8, 0xb0, 0xb0, 0xf0, 0x50 };
const byte PROGMEM knightRightAttack1_bits[] = {
   0x9e, 0x42, 0x36, 0x1f, 0x0d, 0x0d, 0x0f, 0x0a };
const byte PROGMEM knightRightAttack2_bits[] = {
   0x1e, 0x02, 0x16, 0xcf, 0x3d, 0x0d, 0x0f, 0x0a };
const byte PROGMEM knightRightAttack3_bits[] = {
   0x1e, 0x02, 0x16, 0x0f, 0x0d, 0xfd, 0x0f, 0x0a };      
const byte PROGMEM finalCreature_bits[] = {
   0x00, 0x00, 0xc0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xc0, 0x0c,
   0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x48, 0x01, 0x00, 0x00, 0x00,
   0x00, 0xc0, 0xf3, 0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0xe0, 0x07, 0xa4,
   0x02, 0x00, 0x00, 0x00, 0x00, 0xf0, 0xb7, 0x49, 0x05, 0x00, 0x00, 0x00,
   0x00, 0xf8, 0x03, 0x50, 0x05, 0x00, 0x00, 0x00, 0x00, 0xf8, 0xe3, 0x57,
   0x05, 0x00, 0x00, 0x00, 0x00, 0xf8, 0x69, 0x50, 0xf5, 0x01, 0x00, 0x00,
   0x00, 0xf8, 0x61, 0x55, 0x01, 0x06, 0x00, 0x00, 0x00, 0x98, 0x69, 0x50,
   0x04, 0x58, 0x00, 0x00, 0x00, 0x10, 0x63, 0x10, 0xe1, 0xe2, 0x00, 0x00,
   0x00, 0x30, 0xe2, 0x57, 0xac, 0x80, 0x04, 0x00, 0x00, 0x60, 0x06, 0x10,
   0xe3, 0xc2, 0x07, 0x00, 0x00, 0xe0, 0x4f, 0xc9, 0x00, 0xa0, 0x07, 0x00,
   0x00, 0xe0, 0x5f, 0x04, 0x04, 0x80, 0x03, 0x00, 0x00, 0xe0, 0x1f, 0x0a,
   0xcd, 0x0b, 0x02, 0x00, 0x00, 0xc0, 0x3f, 0x49, 0xe3, 0x97, 0x34, 0x00,
   0x00, 0xc0, 0xbf, 0xd8, 0xf6, 0x17, 0x3c, 0x00, 0x00, 0xc0, 0x3f, 0x94,
   0xf1, 0x2f, 0x3e, 0x00, 0x00, 0x80, 0x1f, 0x30, 0xf8, 0x2f, 0x1f, 0x00,
   0x00, 0x00, 0x0f, 0x68, 0xf8, 0x2f, 0x1c, 0x00, 0x00, 0x00, 0x00, 0xe8,
   0xf9, 0x2f, 0x30, 0x00, 0x00, 0x00, 0x00, 0xd0, 0xfb, 0x2f, 0x36, 0x00,
   0x00, 0x00, 0x00, 0xa0, 0xf9, 0x2f, 0xe0, 0x00, 0x00, 0x00, 0x00, 0x20,
   0xf8, 0x17, 0xfe, 0x00, 0x00, 0x00, 0x00, 0x50, 0xf2, 0x17, 0x78, 0x00,
   0x00, 0x00, 0x00, 0x40, 0xf0, 0x0b, 0x60, 0x00, 0x00, 0x00, 0x00, 0x00,
   0xe0, 0x05, 0x40, 0x00, 0x00, 0x00, 0x00, 0x00, 0x0c, 0x80, 0x4c, 0x00,
   0x00, 0x00, 0x00, 0x00, 0x9c, 0x83, 0x50, 0x00, 0x00, 0x00, 0x00, 0x00,
   0xde, 0x41, 0xc0, 0x00, 0x00, 0x00, 0x00, 0x00, 0xde, 0x40, 0xfe, 0x01,
   0x00, 0x00, 0x00, 0x00, 0x1e, 0x20, 0xf8, 0x01, 0x00, 0x00, 0x00, 0x00,
   0x6e, 0x10, 0xe1, 0x00, 0x00, 0x00, 0x00, 0x00, 0x0c, 0x80, 0xcc, 0x00,
   0x00, 0x00, 0x00, 0x00, 0x30, 0x4a, 0x50, 0x00, 0x00, 0x00, 0x00, 0x80,
   0x03, 0x00, 0x42, 0x00, 0x00, 0x00, 0x00, 0xc0, 0x14, 0x0a, 0x41, 0x00,
   0x00, 0x00, 0x00, 0x40, 0x05, 0x00, 0x40, 0x00, 0x00, 0x00, 0x00, 0x58,
   0x06, 0xf8, 0x20, 0x00, 0x20, 0x00, 0x00, 0xbf, 0x03, 0xfc, 0x21, 0x00,
   0x84, 0x00, 0x78, 0x46, 0x40, 0xfe, 0x23, 0x00, 0x8d, 0x09, 0xff, 0x42,
   0xe0, 0xfe, 0x23, 0x00, 0x1a, 0xe3, 0xff, 0x28, 0xe8, 0xfe, 0x13, 0x00,
   0x3c, 0xcb, 0x7f, 0x1a, 0xe8, 0xfe, 0x13, 0x00, 0x70, 0x00, 0x00, 0x00,
   0x4b, 0xfc, 0x09, 0x00 };
void spriteRenderer(int tx, int ty, int ox, int oy, int sprite){
  switch(sprite){
    case -1:
      break;
    case 0: // idleRight1
      SC.drawXBMP(tx*8+ox,ty*8+oy,8,8,idleRight1_bits);
      break;
    case 1: // idleRight2
      SC.drawXBMP(tx*8+ox,ty*8+oy,8,8,idleRight2_bits);
      break;  
    case 2: // jumpRight 
       SC.drawXBMP(tx*8+ox,ty*8+oy,8,8,jumpRight_bits);
      break; 
    case 3: // jumpLeft 
       SC.drawXBMP(tx*8+ox,ty*8+oy,8,8,jumpLeft_bits);
      break; 
    case 4: // fallRight 
       SC.drawXBMP(tx*8+ox,ty*8+oy,8,8,fallRight_bits);
      break; 
    case 5: // fallLeft 
       SC.drawXBMP(tx*8+ox,ty*8+oy,8,8,fallLeft_bits);
      break;   
    case 6: // runRight1 
       SC.drawXBMP(tx*8+ox,ty*8+oy,8,8,runRight1_bits);
      break; 
    case 7: // runRight2 
       SC.drawXBMP(tx*8+ox,ty*8+oy,8,8,runRight2_bits);
      break;  
    case 8: // runLeft1 
       SC.drawXBMP(tx*8+ox,ty*8+oy,8,8,runLeft1_bits);
      break; 
    case 9: // runLeft2 
       SC.drawXBMP(tx*8+ox,ty*8+oy,8,8,runLeft2_bits);
      break;
    case 10: // idleLeft1
      SC.drawXBMP(tx*8+ox,ty*8+oy,8,8,idleLeft1_bits);
      break;
    case 11: // idleLeft2
      SC.drawXBMP(tx*8+ox,ty*8+oy,8,8,idleLeft2_bits); 
      break; 
    case 12: // fullHeart 
       SC.drawXBMP(tx*8+ox,ty*8+oy,8,8,fullHeart_bits);
      break;
    case 13: // emptyHeart
      SC.drawXBMP(tx*8+ox,ty*8+oy,8,8,emptyHeart_bits);
      break;
    case 15:
      SC.drawXBMP(tx*8+ox,ty*8+oy,8,8,skellyIdleRight0_bits);
      break;   
    case 16:
      SC.drawXBMP(tx*8+ox,ty*8+oy,8,8,skellyIdleRight1_bits);
      break;
    case 17:
      SC.drawXBMP(tx*8+ox,ty*8+oy,8,8,skellyIdleLeft0_bits);
      break;   
    case 18:
      SC.drawXBMP(tx*8+ox,ty*8+oy,8,8,skellyIdleLeft1_bits);
      break;  
    case 19:
      SC.drawXBMP(tx*8+ox,ty*8+oy,8,8,skellyRunRight0_bits);
      break;   
    case 20:
      SC.drawXBMP(tx*8+ox,ty*8+oy,8,8,skellyRunRight1_bits);
      break;
    case 21:
      SC.drawXBMP(tx*8+ox,ty*8+oy,8,8,skellyRunLeft0_bits);
      break;   
    case 22:
      SC.drawXBMP(tx*8+ox,ty*8+oy,8,8,skellyRunLeft1_bits);
      break;  
    case 23:
      SC.drawXBMP(tx*8+ox,ty*8+oy,8,8,priestIdle0_bits);
      break;
    case 24:
      SC.drawXBMP(tx*8+ox,ty*8+oy,8,8,priestIdle1_bits);
      break;
    case 25:
      SC.drawXBMP(tx*8+ox,ty*8+oy,8,8,sword1AttackRight_pl_bits);
      break;
    case 26:
      SC.drawXBMP(tx*8+ox,ty*8+oy,8,8,sword1AttackRight_sw_bits);
      break;
    case 27:
      SC.drawXBMP(tx*8+ox,ty*8+oy,8,8,sword1AttackLeft_pl_bits);
      break;
    case 28:
      SC.drawXBMP(tx*8+ox,ty*8+oy,8,8,sword1AttackLeft_sw_bits);
      break;
    case 29:
      SC.drawXBMP(tx*8+ox,ty*8+oy,8,8,staff1AttackRight_pl_bits);
      break;
    case 30:
      SC.drawXBMP(tx*8+ox,ty*8+oy,8,8,staff1AttackRight_sw_bits);
      break;
    case 31:
      SC.drawXBMP(tx*8+ox,ty*8+oy,8,8,staff1AttackLeft_pl_bits);
      break;
    case 32:
      SC.drawXBMP(tx*8+ox,ty*8+oy,8,8,staff1AttackLeft_sw_bits);
      break;
    case 33:
      SC.drawXBMP(tx*8+ox,ty*8+oy,8,8,staff1AttackTopRight_sw_bits);
      break;
    case 34:
      SC.drawXBMP(tx*8+ox,ty*8+oy,8,8,staff1AttackTopLeft_sw_bits);
      break;
    case 35:
      SC.drawXBMP(tx*8+ox,ty*8+oy,8,8,fireballRight1_bits);
      break;
    case 36:
      SC.drawXBMP(tx*8+ox,ty*8+oy,8,8,fireballRight2_bits);
      break;
    case 37:
      SC.drawXBMP(tx*8+ox,ty*8+oy,8,8,fireballLeft1_bits);
      break;
    case 38:
      SC.drawXBMP(tx*8+ox,ty*8+oy,8,8,fireballLeft2_bits);
      break;
    case 39:
      SC.drawXBMP(tx*8+ox,ty*8+oy,8,8,arrowLeft_bits);
      break;
    case 40:
      SC.drawXBMP(tx*8+ox,ty*8+oy,8,8,arrowRight_bits);
      break;
    case 41:
      SC.drawXBMP(tx*8+ox,ty*8+oy,8,8,bowAttackRight_pl_bits);
      break;
    case 42:
      SC.drawXBMP(tx*8+ox,ty*8+oy,8,8,bowAttackRight_sw_bits);
      break;
    case 43:
      SC.drawXBMP(tx*8+ox,ty*8+oy,8,8,bowAttackLeft_pl_bits);
      break;
    case 44:
      SC.drawXBMP(tx*8+ox,ty*8+oy,8,8,bowAttackLeft_sw_bits);
      break;
    case 45:
      SC.drawXBMP(tx*8+ox,ty*8+oy,8,8,batFlyRight1_bits);
      break;   
    case 46:
      SC.drawXBMP(tx*8+ox,ty*8+oy,8,8,batFlyRight2_bits);
      break;   
    case 47:
      SC.drawXBMP(tx*8+ox,ty*8+oy,8,8,batFlyLeft1_bits);
      break;   
    case 48:
      SC.drawXBMP(tx*8+ox,ty*8+oy,8,8,batFlyLeft2_bits);
      break;   
    case 51:
      SC.drawXBMP(tx*8+ox,ty*8+oy,8,8,potionItem1_bits); 
      break;     
    case 52:
      SC.drawXBMP(tx*8+ox,ty*8+oy,8,8,keyItem1_bits); 
      break; 
    case 53:
      SC.drawXBMP(tx*8+ox,ty*8+oy,8,8,swordItem1_bits); 
      break; 
    case 54:
      SC.drawXBMP(tx*8+ox,ty*8+oy,8,8,helmetItem1_bits); 
      break; 
    case 55:
      SC.drawXBMP(tx*8+ox,ty*8+oy,8,8,chainBottom1_bits); 
      break; 
    case 56:
      SC.drawXBMP(tx*8+ox,ty*8+oy,8,8,chainMiddle1_bits); 
      break; 
    case 57:
      SC.drawXBMP(tx*8+ox,ty*8+oy,8,8,chainTop1_bits); 
      break; 
    case 58:
      SC.drawXBMP(tx*8+ox,ty*8+oy,8,8,chestLeftClosed_bits);
      break;
    case 59:
      SC.drawXBMP(tx*8+ox,ty*8+oy,8,8,chestLeftOpen_bits);
      break;
    /*case 61:
      SC.drawXBMP(tx*8+ox,ty*8+oy,8,8,wallTorch1_bits); 
      break; 
    case 62:
      SC.drawXBMP(tx*8+ox,ty*8+oy,8,8,wallTorch2_bits); 
      break; 
    case 63:
      SC.drawXBMP(tx*8+ox,ty*8+oy,8,8,wallTorch3_bits); 
      break; 
    case 64:
      SC.drawXBMP(tx*8+ox,ty*8+oy,8,8,wallTorch4_bits); 
      break; 
    case 65:
     SC.drawXBMP(tx*8+ox,ty*8+oy,8,8,wallTorch5_bits); 
     break;*/ 
    case 66:
      SC.drawXBMP(tx*8+ox,ty*8+oy,8,8,boots1_bits);
      break;
    case 67:
      SC.drawXBMP(tx*8+ox,ty*8+oy,8,8,jar_bits);
      break;   
    case 68:
      SC.drawXBMP(tx*8+ox,ty*8+oy,8,8,scroll_bits);
      break; 
    case 69:
      SC.drawXBMP(tx*8+ox,ty*8+oy,8,8,exclamation_bits);
      break; 
    case 70:
      SC.drawXBMP(tx*8+ox,ty*8+oy,8,8,staff1_bits);
      break; 
    case 71:
      SC.drawXBMP(tx*8+ox,ty*8+oy,8,8,bow_bits);
      break; 
    case 72:
      SC.drawXBMP(tx*8+ox,ty*8+oy,8,8,turkey_bits);
      break; 
    case 73:
      SC.drawXBMP(tx*8+ox,ty*8+oy,8,8,turkeyEaten_bits);
      break; 
    case 74:
      SC.drawXBMP(tx*8+ox,ty*8+oy,8,8,chairLeft_bits);
      break; 
    case 75:
      SC.drawXBMP(tx*8+ox,ty*8+oy,8,8,chairRight_bits);
      break; 
    case 76:
      SC.drawXBMP(tx*8+ox,ty*8+oy,10,8,flowers0_bits);
      break; 
    case 77:
      SC.drawXBMP(tx*8+ox,ty*8+oy,10,8,flowers1_bits);
      break; 
    case 78:
      SC.drawXBMP(tx*8+ox,ty*8+oy,10,8,flowers2_bits);
      break; 
    case 79:
     SC.drawXBMP(tx*8+ox,ty*8+oy,8,8,skellyBone_bits);
     break;
    case 94:
      SC.drawXBMP(tx*8+ox,ty*8+oy,8,8,leverOn_bits);
      break; 
    case 95:
      SC.drawXBMP(tx*8+ox,ty*8+oy,8,8,leverOff_bits);
      break; 
    case 96:
      SC.drawXBMP(tx*8+ox,ty*8+oy,8,16,platformIdle0_bits);
      break; 
    case 97:
      SC.drawXBMP(tx*8+ox,ty*8+oy,8,16,platformIdle1_bits);
      break; 
    case 98:
      SC.drawXBMP(tx*8+ox,ty*8+oy,8,16,platformLeft_bits);
      break; 
    case 99:
      SC.drawXBMP(tx*8+ox,ty*8+oy,8,16,platformRight_bits);
      break;
    case 100: // rockFloor
      SC.drawXBMP(tx*8+ox,ty*8+oy,8,8,rockFloor_bits);
      break;
    case 101: // water
        SC.drawXBMP(tx*8+ox,ty*8+oy,8,8,water1_bits);
        break;
    case 111:
      SC.drawXBMP(tx*8+ox,ty*8+oy,8,8,eyeCenter1_bits);
      break;    
    case 112:
      SC.drawXBMP(tx*8+ox,ty*8+oy,8,8,eyeCenter2_bits);
      break;   
    case 113:
      SC.drawXBMP(tx*8+ox,ty*8+oy,8,8,eyeLeft1_bits);
      break;   
    case 114:
      SC.drawXBMP(tx*8+ox,ty*8+oy,8,8,eyeLeft2_bits);
      break;   
    case 115:
      SC.drawXBMP(tx*8+ox,ty*8+oy,8,8,eyeRight1_bits);
      break;   
    case 116:
      SC.drawXBMP(tx*8+ox,ty*8+oy,8,8,eyeRight2_bits);
      break;   
    case 117:
      SC.drawXBMP(tx*8+ox,ty*8+oy,8,8,tearDropLeft1_bits);
      break;   
    case 118:
      SC.drawXBMP(tx*8+ox,ty*8+oy,8,8,tearDropRight1_bits);
      break;  
    case 119:
      SC.drawXBMP(tx*8+ox,ty*8+oy,8,8,snakeIdleLeft0_bits);
      break;   
    case 120:
      SC.drawXBMP(tx*8+ox,ty*8+oy,8,8,snakeIdleLeft1_bits);
      break;  
    case 121:
      SC.drawXBMP(tx*8+ox,ty*8+oy,8,8,snakeIdleRight0_bits);
      break;   
    case 122:
      SC.drawXBMP(tx*8+ox,ty*8+oy,8,8,snakeIdleRight1_bits);
      break;  
    case 123:
      SC.drawXBMP(tx*8+ox,ty*8+oy,8,8,snakeRunLeft2_bits);
      break;   
    case 124:
      SC.drawXBMP(tx*8+ox,ty*8+oy,8,8,snakeRunLeft3_bits);
      break;  
    case 125:
      SC.drawXBMP(tx*8+ox,ty*8+oy,8,8,snakeRunLeft4_bits);
      break;   
    case 126:
      SC.drawXBMP(tx*8+ox,ty*8+oy,8,8,snakeRunRight2_bits);
      break;  
    case 127:
      SC.drawXBMP(tx*8+ox,ty*8+oy,8,8,snakeRunLeft3_bits);
      break;   
    case 128:
      SC.drawXBMP(tx*8+ox,ty*8+oy,8,8,snakeRunLeft4_bits);
      break;  
    case 200:
      SC.drawXBMP(tx*8+ox,ty*8+oy,8,8,barrel_bits);
      break;   
    case 201:
      SC.drawXBMP(tx*8+ox,ty*8+oy,8,24,banner_bits);
      break; 
    case 202:
      SC.drawXBMP(tx*8+ox,ty*8+oy,8,8,knightIdleRight0_bits);
      break;   
    case 203:
      SC.drawXBMP(tx*8+ox,ty*8+oy,8,8,knightIdleRight1_bits);
      break; 
    case 204:
      SC.drawXBMP(tx*8+ox,ty*8+oy,8,8,knightIdleLeft0_bits);
      break;   
    case 205:
      SC.drawXBMP(tx*8+ox,ty*8+oy,8,8,knightIdleLeft1_bits);
      break;
    case 206:
      SC.drawXBMP(tx*8+ox,ty*8+oy,8,8,knightLeftAttack1_bits);
      break;   
    case 207:
      SC.drawXBMP(tx*8+ox,ty*8+oy,8,8,knightLeftAttack2_bits);
      break; 
    case 208:
      SC.drawXBMP(tx*8+ox,ty*8+oy,8,8,knightLeftAttack3_bits);
      break;   
    case 209:
      SC.drawXBMP(tx*8+ox,ty*8+oy,8,8,knightRightAttack1_bits);
      break;
    case 210:
      SC.drawXBMP(tx*8+ox,ty*8+oy,8,8,knightRightAttack2_bits);
      break; 
    case 211:
      SC.drawXBMP(tx*8+ox,ty*8+oy,8,8,knightRightAttack3_bits);
      break;
    case 222:
      SC.drawXBMP(tx*8+ox,ty*8+oy,12,10,drainEntranceClosed_bits);
      break;   
    case 223:
      SC.drawXBMP(tx*8+ox,ty*8+oy,12,10,drainEntranceOpen_bits);
      break;
    case 224:
      SC.drawXBMP(tx*8+ox,ty*8+oy,16,16,windowEmpty_bits);
      break; 
    case 225:
      SC.drawXBMP(tx*8+ox,ty*8+oy,16,16,windowStars_bits);
      break;   
    case 226:
      SC.drawXBMP(tx*8+ox,ty*8+oy,12,11,telescopeClosed_bits);
      break; 
    case 227:
      SC.drawXBMP(tx*8+ox,ty*8+oy,12,11,telescopeOpen_bits);
      break;   
    case 228:
      SC.drawXBMP(tx*8+ox,ty*8+oy,9,11,globe1_bits);
      break;
    case 229:
      SC.drawXBMP(tx*8+ox,ty*8+oy,9,11,globe2_bits);
      break;   
    case 230:
      SC.drawXBMP(tx*8+ox,ty*8+oy,9,11,globe3_bits);
      break;
    case 254:
      SC.drawXBMP(tx*8+ox,ty*8+oy,57,47,finalCreature_bits);
      break;
    default:
      break;        
  }
}
