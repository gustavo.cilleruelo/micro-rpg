
uint8_t rectangleCollision(uint8_t x1, uint8_t y1, uint8_t w1, uint8_t h1,uint8_t x2, uint8_t y2, uint8_t w2, uint8_t h2){
  int rightColl = x1 + w1 - x2;
  int leftColl = x2 + w2 - x1; 
  int bottColl = y1 + h1 - y2;
  int topColl = y2 + h2 - y1;
  if(leftColl >= 0 && rightColl >= 0 && bottColl >= 0 && topColl >= 0){
    return 1;
  }
  return 0;
}

uint8_t debugPrint(uint8_t bug, uint8_t x, uint8_t y){
  SC.setCursor(x,y);
  SC.print(bug);
  SC.sendBuffer();
}

uint8_t toPixel(uint8_t tile, uint8_t offs){
  return tile*8 + offs;
}
uint8_t toTile(uint8_t pixel){
  return pixel/8;
}
uint8_t toOffs(uint8_t pixel){
  return pixel%8;
}


uint8_t power(uint8_t base, uint8_t exp){
    uint8_t pow = 1;
    for(int i = 0; i < exp; ++i){
        pow = pow*base;
    }
    return pow;
}
//void toBinary(uint8_t* vec, uint8_t n){ 
void toBinary(uint8_t (&vec)[8], uint8_t n){ 
  for(int i = 0; i < 8; ++i){
    vec[8-i] = n%2;
    n = n/2;
  } 
  return;
}
// el vector vec es el binario al revés!
uint8_t toInt8(uint8_t (&vec)[8]){
    uint8_t sum = 0;
    for(int i = 0; i < 8; ++i){
        sum = sum + vec[i]*pow(2,i);
    }
    return sum;
}

/*void debugSerialVec(int i, uint8_t* vec){
  Serial.println("----");
  Serial.println(i);
  Serial.println("-");
  for(int i = 0; i < 8; ++i){
    Serial.println(vec[i]);
  }    
}*/
