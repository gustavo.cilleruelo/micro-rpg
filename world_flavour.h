////// PRIEST MUCHO TEXT0 //////
uint8_t text = 0;
const char PROGMEM gameName[] = {"microRPG"};
const char PROGMEM studioName[] = {"by androm_na"};
const char PROGMEM mission1[] = {":)"};
const char PROGMEM gameOver[] = {"gameOver"};
const char PROGMEM enterTheDungeon[] = {"enterTheDungeon"};
const char PROGMEM credits[] = {"credits"};
const char PROGMEM leaderboard[] = {"highScores"};
const char PROGMEM gus[] = {"dev: @alpinemoose"};
const char PROGMEM bernat[] = {"console: @_vernsace"};
const char PROGMEM art[] = {"~ artCredits ~"};
const char PROGMEM art1[] = {"@spaceman_lepe"};
const char PROGMEM art2[] = {"@brandonjamesgreer"};
const char PROGMEM art3[] = {"@vurmux"};
void priestExplainMission(){
  text = 1;
}
void drawText(uint8_t x, uint8_t y){
  SC.setCursor(x,y);
  switch(text){
    case 21:
      SC.print((class __FlashStringHelper *)leaderboard);
      break;
    case 22:
      SC.print((class __FlashStringHelper *)gameName);
      break;
    case 23:
      SC.print((class __FlashStringHelper *)studioName);
      break;    
    case 24:
      SC.print((class __FlashStringHelper *)enterTheDungeon);
      break;
    case 25:
      SC.print((class __FlashStringHelper *)credits);
      break;    
    case 26:
      SC.print((class __FlashStringHelper *)gus);
      break;
    case 27:
      SC.print((class __FlashStringHelper *)bernat);
      break;    
    case 28:
      SC.print((class __FlashStringHelper *)art);
      break;
    case 29:
      SC.print((class __FlashStringHelper *)art1);
      break;    
    case 30:
      SC.print((class __FlashStringHelper *)art2);
      break;
    case 31:
      SC.print((class __FlashStringHelper *)art3);
      break;    
    case 1:
      SC.print((class __FlashStringHelper *)mission1);
      break;
    case 44:
      SC.print((class __FlashStringHelper *)gameOver);
      break;
    default:
      break;
  }
}


////// ITEMS //////

uint8_t itemType(uint8_t item){
    // 0 : utility
    // 1 : weapon
    // 2 : boots
    // 3 : helmet
  switch(item){
    case 51: // potion 1
        return 0;
    case 52: // key 1
      return 0;
    case 53: // sword 1
      return 1;
    case 54: // helmet 1
      return 3;
    case 66: // boots 1
      return 2;
    case 68: // scroll
      return 0;
    case 70: // staff 1
      return 1;
    case 71: // bow
      return 1;
    case 79: // bone
      return 0;
  }
}
uint8_t lootBarrelDrain(){
  uint8_t rand = random(0,100);
  if(rand > 95){
    return 52; // key
  }
  if(rand > 90){
    return 53; // helmet
  }  
  if(rand > 80){
    return 51; // potion
  }
  return 68; 
}
uint8_t lootBarrelCorridor(){
  uint8_t rand = random(0,100);
  if(rand > 90){
    return 66; // boots
  }
  if(rand > 80){
    return 54; // helmet
  }  
  if(rand > 50){
    return 51; // potion
  }
  return 0; 
}
uint8_t lootChestCastle(){
  uint8_t rand = random(0,100);
  if(rand > 80){
    return 68; // scroll
  }
  if(rand > 50){
    return 51; // potion
  }
  return 0; 
}
uint8_t randomLoot(uint8_t theme, uint8_t objId){
  if(objId == 58){ // barrel/chest
    if(theme == 0){return lootBarrelCorridor();}
    if(theme == 11 or theme == 17){return lootChestCastle();}
    if(theme == 12){return lootBarrelDrain();}
  }
  return 0;
}